package obj

import (
	"bufio"
	"errors"
	"strconv"
	"strings"
)

var TooManyArguments = errors.New("too many arguments")
var NotEnoughArguments = errors.New("not enough arguments")

func parseTextureVec(line []byte, value *[]float64, values int) error {
	i := 0
	for ; i < len(line) && line[i] != ' '; i++ {
	}
	if line[i] != ' ' || (len(line)-1)-i == 0 {
		return NotEnoughArguments
	}
	vec, err := parseVec(string(line[i+1:]), values)
	if err != nil {
		return err
	}
	*value = vec
	return nil
}

func parseTextureFloat(line []byte, value *float64) error {
	i := 0
	for ; i < len(line) && line[i] != ' '; i++ {
	}
	if line[i] != ' ' || (len(line)-1)-i == 0 {
		return NotEnoughArguments
	}
	num, err := parseFloat(string(line[i+1:]))
	if err != nil {
		return err
	}
	*value = num
	return nil
}

func parseTextureInt(line []byte, value *int64) error {
	i := 0
	for ; i < len(line) && line[i] != ' '; i++ {
	}
	if line[i] != ' ' || (len(line)-1)-i == 0 {
		return NotEnoughArguments
	}
	num, err := parseInt(string(line[i+1:]))
	if err != nil {
		return err
	}
	*value = num
	return nil
}

func parseTextureName(line []byte, texture *Texture) error {
	i := 0
	for ; i < len(line) && line[i] != ' '; i++ {
	}
	if line[i] != ' ' || (len(line)-1)-i == 0 {
		return NotEnoughArguments
	}
	texture.Name = string(line[i+1:])
	return nil
}

func parseVec(data string, items int) ([]float64, error) {
	vec := make([]float64, items)
	reader := strings.NewReader(data)
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanWords)
	iter := 0
	for scanner.Scan() {
		if iter > items - 1 {
			return nil, TooManyArguments
		}
		num, err := parseFloat(scanner.Text())
		if err != nil {
			return nil, err
		}
		vec[iter] = num
		iter++
	}
	if iter < items-1 {
		return nil, NotEnoughArguments
	}
	return vec, nil
}

func parseFloat(data string) (float64, error) {
	num, err := strconv.ParseFloat(data, 64)
	return num, err
}

func parseInt(data string) (int64, error) {
	num, err := strconv.ParseInt(data, 10, 64)
	return num, err
}

func parseUint(data string) (uint64, error) {
	num, err := strconv.ParseUint(data, 10, 64)
	return num, err
}
