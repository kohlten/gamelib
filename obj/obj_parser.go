package obj

import (
	"errors"
	"fmt"
	"io/ioutil"
)

// @TODO Obj files can also be a binary file with a .mod extension. (Does mtl have a binary version?)

const (
	// Vertex Data
	geometricVertices      = "v"
	textureVertices        = "vt"
	parameterSpaceVertices = "vp"
	normalVertices         = "vn"
	curveType              = "cstype"
	degree                 = "deg"
	basisMatrix            = "bmat"
	stepSize               = "step"

	// Elements
	point   = "p"
	line    = "l"
	polygon = "f"
	curve   = "curv"
	curve2d = "curv2"
	surface = "surf"

	// Free-form curve/surface body statements
	parameterValues   = "parm"
	outerTrimmingLoop = "trim"
	innerTrimmingLoop = "hole"
	specialCurve      = "scrv"
	specialPoint      = "sp"
	endStatement      = "end"

	// Connectivity between free-form surfaces
	connect = "con"

	// Grouping
	groupName      = "g"
	smoothingGroup = "s"
	mergingGroup   = "mg"
	objectName     = "o"

	// Display/render attributes
	bevelInterpolation            = "bevel"
	colorInterpolation            = "c_interp"
	dissolveInterpolation         = "d_interp"
	levelOfDetail                 = "lod"
	materialName                  = "usemtl"
	materialLibrary               = "mtllib"
	shadowCasting                 = "shadow_obj"
	rayTracing                    = "trace_obj"
	curveApproximationTechnique   = "ctech"
	surfaceApproximationTechnique = "stech"

	// General
	runFile           = "call"
	frameSubstitution = "scmp"
	executeShell      = "csh"
)

type Obj struct {
	Name                   string
	GeometricVertices      [][]float64
	TextureVertices        [][]float64
	VertexNormals          [][]float64
	ParameterSpaceVertices [][]float64
	Polygons               [][]uint64
	Lines                  [][]uint64
	Material               *Texture
	Smoothed               bool
}

type Manager struct {
	Objs     []*Obj
	Textures map[string]*Texture
}

var basicCommands = map[string]func(data []string, manager *Manager) (uint64, error){
	materialLibrary: loadMtl,
	objectName:      newObj,
}

var objectCommands = map[string]func(data string, manager *Manager, obj *Obj) error{
	geometricVertices:      parseGeometricVertices,
	parameterSpaceVertices: parseParameterSpaceVertices,
	normalVertices:         parseNormals,
	textureVertices:        parseTextureVertices,
	materialName:           parseObjMaterial,
	smoothingGroup:         parseSmoothing,
	polygon:                parsePolygon,
	line:                   parseLine,
}

func parseObj(data []byte) (*Manager, error) {
	manager := new(Manager)
	manager.Textures = make(map[string]*Texture)
	lines := splitLines(data)
	lines = removeComments(lines)
	for {
		if len(lines) == 0 {
			break
		}
		fn, ok := basicCommands[getCommandName([]byte(lines[0]))]
		if ok {
			linesParsed, err := fn(lines, manager)
			if err != nil {
				return nil, err
			}
			lines = lines[linesParsed:]
		} else {
			return nil, errors.New(fmt.Sprintf("unknown command %s", getCommandName([]byte(lines[0]))))
		}
	}
	return manager, nil
}

func LoadObj(filename string) (*Manager, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	manager, err := parseObj(data)
	if err != nil {
		return nil, err
	}
	return manager, nil
}

func LoadObjFromBytes(data []byte) (*Manager, error) {
	manager, err := parseObj(data)
	if err != nil {
		return nil, err
	}
	return manager, nil
}
