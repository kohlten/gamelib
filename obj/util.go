package obj

import (
	"bufio"
	"bytes"
	"strings"
)

func FindNext(data []byte, substring []byte) (found bool, location int) {
	substringLength := len(substring)
	for i := 0; i < len(data); i++ {
		k := i
		for j := 0; j < substringLength; j++ {
			if substring[j] != data[k] {
				break
			}
			k++
		}
		if k-i == substringLength {
			return true, i
		}
	}
	return false, -1
}

func splitLines(data []byte) []string {
	reader := bytes.NewReader(data)
	scanner := bufio.NewScanner(reader)
	lines := make([]string, 0)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

func splitWords(data string) []string {
	reader := strings.NewReader(data)
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanWords)
	words := make([]string, 0)
	for scanner.Scan() {
		words = append(words, scanner.Text())
	}
	return words
}

func removeComments(data []string) []string {
	for i := 0; i < len(data); {
		if data[i][0] == '#' {
			data = append(data[:i], data[i+1:]...)
		} else {
			i++
		}
	}
	return data
}

func getCommandName(line []byte) string {
	i := 0
	for ; i < len(line) && line[i] != ' '; i++ {
	}
	return string(line[:i])
}
