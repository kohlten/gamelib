package obj

import (
	"errors"
	"fmt"
	"strings"
)

var noObjName = errors.New("object starts without a name")

func loadMtl(data []string, manager *Manager) (uint64, error) {
	words := splitWords(data[0])
	mtls, err := LoadMtl(words[1])
	if err != nil {
		return 0, err
	}
	for i := 0; i < len(mtls); i++ {
		manager.Textures[mtls[i].Name] = mtls[i]
	}
	return 1, nil
}

func parseName(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) != 2 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", objectName, len(words), 2))
	}
	obj.Name = words[1]
	return nil
}

// @TODO DON'T COPY CODE
func parseGeometricVertices(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 4 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 4))
	}
	vertex := make([]float64, 0)
	err := parseTextureVec([]byte(data[1:]), &vertex, len(words)-1)
	if err != nil {
		return err
	}
	if len(vertex) == 3 {
		vertex = append(vertex, 1.0)
	}
	obj.GeometricVertices = append(obj.GeometricVertices, vertex)
	return nil
}

func parseParameterSpaceVertices(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 3 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 3))
	}
	vertex := make([]float64, 0)
	err := parseTextureVec([]byte(data[1:]), &vertex, len(words)-1)
	if err != nil {
		return err
	}
	if len(vertex) == 2 {
		vertex = append(vertex, 1.0)
	}
	obj.ParameterSpaceVertices = append(obj.ParameterSpaceVertices, vertex)
	return nil
}

func parseNormals(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 3 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 3))
	}
	vertex := make([]float64, 0)
	err := parseTextureVec([]byte(data[1:]), &vertex, len(words)-1)
	if err != nil {
		return err
	}
	if len(vertex) == 2 {
		vertex = append(vertex, 1.0)
	}
	obj.VertexNormals = append(obj.VertexNormals, vertex)
	return nil
}

func parseTextureVertices (data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 3 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 3))
	}
	vertex := make([]float64, 0)
	err := parseTextureVec([]byte(data[1:]), &vertex, len(words)-1)
	if err != nil {
		return err
	}
	if len(vertex) == 2 {
		vertex = append(vertex, 1.0)
	}
	obj.TextureVertices = append(obj.TextureVertices, vertex)
	return nil
}

// @TODO---------------------------------------------------------------------------------------------------------------------------

func parseObjMaterial(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 2 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 2))
	}
	material, ok := manager.Textures[words[1]]
	if !ok {
		return errors.New(fmt.Sprintf("texture %s has not been loaded", words[1]))
	}
	obj.Material = material
	return nil
}

func parseSmoothing(data string, manager *Manager, obj *Obj) error {
	words := splitWords(data)
	if len(words) < 2 {
		return errors.New(fmt.Sprintf("command %s has %d arguments, expected %d arguments", words[0], len(words), 2))
	}
	smoothed, err := parseInt(words[1])
	if err != nil {
		if words[1] == "off" {
			obj.Smoothed = false
		} else if words[1] == "on" {
			obj.Smoothed = true
		} else {
			return err
		}
		return nil
	}
	if smoothed > 0 {
		obj.Smoothed = true
	} else {
		obj.Smoothed = false
	}
	return nil
}

func parsePolygon(data string, manager *Manager, obj *Obj) error {
	vecs := make([]uint64, 0)
	words := splitWords(data)
	for i := 1; i < len(words); i++ {
		words[i] = strings.Replace(words[i], "/", " ", -1)
		vecF, err := parseVec(words[i], 3)
		if err != nil {
			return err
		}
		vec := make([]uint64, len(vecF))
		for i := 0; i < len(vecF); i++ {
			vec[i] = uint64(vecF[i])
		}
		vecs = append(vecs, vec...)
	}
	obj.Polygons = append(obj.Polygons, vecs)
	return nil
}

func parseLine(data string, manager *Manager, obj *Obj) error {
	lines := make([]uint64, 0)
	words := splitWords(data)
	for i := 1; i < len(words); i++ {
		words[i] = strings.Replace(words[i], "/", " ", -1)
		lineF, err := parseVec(words[i], 2)
		if err != nil {
			return err
		}
		line := make([]uint64, len(lineF))
		for i := 0; i < len(lineF); i++ {
			line[i] = uint64(lineF[i])
		}
		lines = append(lines, line...)
	}
	obj.Lines = append(obj.Lines, lines)
	return nil
}

func newObj(data []string, manager *Manager) (uint64, error) {
	obj := new(Obj)
	err := parseName(data[0], manager, obj)
	if err != nil {
		return 0, err
	}
	i := 1
	for ; i < len(data); i++ {
		words := splitWords(data[i])
		fn, ok := objectCommands[words[0]]
		if words[0] == objectName {
			break
		}
		if !ok {
			return 0, errors.New(fmt.Sprintf("command %s was not found", words[0]))
		}
		err = fn(data[i], manager, obj)
		if err != nil {
			return 0, err
		}
	}
	manager.Objs = append(manager.Objs, obj)
	return uint64(i), nil
}
