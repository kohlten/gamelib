package obj

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
)

// @TODO Each value in the texture can also accept a file for the data
// @TODO Ka is also defined with spectral and xyz formats.
//  	The formats are mutually exclusive and must present an error when they are in the material more than once.
// @TODO Add errors for out of bounds and invalid commands
// @TODO Values like o s and t have optional arguments
// @TODO -d has an argument called "halo"
// @TODO Add parsing for relf -type

var noMaterials = errors.New("no materials found in file")
var commandNotFound = errors.New("command was not found")

const (
	commandNew                 = "newmtl"
	commandAmbient             = "Ka"
	commandDiffuse             = "Kd"
	commandSpecular            = "Ks"
	commandTransmissionFilter  = "Tf"
	commandOpticalDensity      = "Ni"
	commandSpecularExponent    = "Ns"
	commandTransparencyD       = "d"
	commandTransparencyTr      = "Tr"
	commandSharpness           = "sharpness"
	commandIllumination        = "illum"
	commandMapAmbient          = "map_Ka"
	commandMapDiffuse          = "map_Kd"
	commandMapSpecular         = "map_Ks"
	commandMapSpecularExponent = "map_Ns"
	commandMapTransparency     = "map_d"
	commandMapBump             = "map_Bump"
	commandBump                = "bump"
	commandDisplacement        = "disp"
	commandDecal               = "decal"
	commandReflection          = "refl"
)

const (
	commandTextureMapBlendHorizontal   = "blendu"
	commandTextureMapBlendVertical     = "blendv"
	commandTextureMapColorCorrection   = "cc"
	commandTextureMapBoost             = "boost"
	commandTextureMapMM                = "mm"
	commandTextureMapOriginOffset      = "o"
	commandTextureMapScale             = "s"
	commandTextureMapTurbulence        = "t"
	commandTextureMapTextureResolution = "texres"
	commandTextureMapClamp             = "clamp"
	commandTextureMapBumpMultiplier    = "bm"
	commandTextureMapChannel           = "imfchan"
	commandTextureMapReflectionType    = "type"
)

const (
	TextureMapTypeKa           = 0
	TextureMapTypeKd           = 1
	TextureMapTypeKs           = 2
	TextureMapTypeNs           = 3
	TextureMapTypeD            = 4
	TextureMapTypeBump         = 5
	TextureMapTypeDisplacement = 6
	TextureMapTypeDecal        = 7
	TextureMapTypeReflection   = 8
)

type TextureMapType int

// R = Red
// G = Green
// B = Blue
// M = matte
// L = luminance
// Z = z - depth
const (
	ChannelR = 0
	ChannelG = 1
	ChannelB = 2
	ChannelM = 3
	ChannelL = 4
	ChannelZ = 5
)

type TextureChannel int

const (
	TypeSphere     = 0
	TypeCubeTop    = 1
	TypeCubeBottom = 2
	TypeCubeFront  = 3
	TypeCubeBack   = 4
	TypeCubeLeft   = 5
	TypeCubeRight  = 6
)

type ReflectionType int

type TextureMap struct {
	MapType           TextureMapType
	BlendHorizontal   bool
	BlendVertical     bool
	ColorCorrection   bool
	Boost             float64
	MM                []float64 // 2
	OriginOffset      []float64 // 3
	Scale             []float64 // 3
	Turbulence        []float64 // 3
	TextureResolution uint64
	Clamp             bool
	BumpMultiplier    float64
	Channel           TextureChannel
	ReflectionType    ReflectionType
	Filename          string
}

const (
	IlluminationModeColorOnAmbientOff                = 0
	IlluminationModeColorOnAmbientOn                 = 1
	IlluminationModeHighlightOn                      = 2
	IlluminationModeReflectionOnRayTraceOn           = 3
	IlluminationModeGlassOnRayTraceOn                = 4
	IlluminationModeFresnelOnRayTraceOn              = 5
	IlluminationModeRefractionOnFresnelOffRayTraceOn = 6
	IlluminationModeRefractionOnFresnelOnRayTraceOn  = 7
	IlluminationModeReflectionOnRayTraceOff          = 8
	IlluminationModeGlassOnRayTraceOff               = 9
	IlluminationModeCastShadowsOnInvisibleSurfaces   = 10
)

type IlluminationMode uint8

type Texture struct {
	Name                string
	Ambient             []float64 // 3
	Diffuse             []float64 // 3
	Specular            []float64 // 3
	TransmissionFilter  []float64
	OpticalDensity      float64
	SpecularExponent    float64
	Transparency        float64
	Sharpness           int64
	Illumination        IlluminationMode
	MapAmbient          TextureMap
	MapDiffuse          TextureMap
	MapSpecular         TextureMap
	MapSpecularExponent TextureMap
	MapTransparency     TextureMap
	MapBump             TextureMap
	Displacement        TextureMap
	Decal               TextureMap
	Reflection          TextureMap
}

const (
	DefaultTransparency = 1.0
	DefaultSharpness    = 60
)

func newTexture() *Texture {
	texture := new(Texture)
	texture.Transparency = DefaultTransparency
	texture.Sharpness = DefaultSharpness
	return texture
}

var commandPath = map[string]func([]byte, *Texture) error{
	commandNew:                parseTextureName,
	commandAmbient:            parseAmbient,
	commandDiffuse:            parseDiffuse,
	commandSpecular:           parseSpecular,
	commandTransmissionFilter: parseTransmissionFilter,
	commandOpticalDensity:     parseOpticalDensity,
	commandSpecularExponent:   parseSpecularExponent,
	commandTransparencyD:      parseTransparency,
	commandTransparencyTr:     parseTransparency,
	commandSharpness:          parseSharpness,
	commandIllumination:       parseIllumination,

	commandMapAmbient:          parseMapAmbient,
	commandMapDiffuse:          parseMapDiffuse,
	commandMapSpecular:         parseMapSpecular,
	commandMapSpecularExponent: parseMapSpecularExponent,
	commandMapTransparency:     parseMapTransparency,
	commandMapBump:             parseBump,
	commandBump:                parseBump,
	commandDisplacement:        parseDisp,
	commandDecal:               parseDecal,
	commandReflection:          parseReflection,
}

func parseCommand(line []byte, texture *Texture) error {
	parseFunc, ok := commandPath[getCommandName(line)]
	if !ok {
		return commandNotFound
	}
	return parseFunc(line, texture)
}

func parseMaterial(scanner *bufio.Scanner) (*Texture, error) {
	texture := newTexture()
	for scanner.Scan() {
		line := scanner.Bytes()
		if len(line) > 0 {
			err := parseCommand(line, texture)
			if err == commandNotFound {
				fmt.Printf("command: '%s' could not be parsed\n", string(line))
			} else if err != nil {
				return nil, err
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return texture, nil
}

func parseMtl(fileData []byte) ([]*Texture, error) {
	textures := make([]*Texture, 0)
	for {
		found, matStart := FindNext(fileData, []byte(commandNew))
		if !found {
			return nil, noMaterials
		}
		found, matEnd := FindNext(fileData[matStart+len(commandNew):], []byte(commandNew))
		if !found {
			matEnd = len(fileData)
		} else {
			matEnd += matStart + len(commandNew)
		}
		data := fileData[matStart:matEnd]
		reader := bytes.NewReader(data)
		scanner := bufio.NewScanner(reader)
		texture, err := parseMaterial(scanner)
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
		fileData = fileData[matEnd:]
		if len(fileData) <= 1 {
			break
		}
	}
	return textures, nil
}

func LoadMtl(filename string) ([]*Texture, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	textures, err := parseMtl(data)
	if err != nil {
		return nil, err
	}
	return textures, nil
}

func LoadMtlFromBytes(data []byte) ([]*Texture, error) {
	textures, err := parseMtl(data)
	if err != nil {
		return nil, err
	}
	return textures, nil
}
