// Heavily based off of the flag library. I created this because I needed multiple values in one flag and did not
// need to use the = operator. Currently this is not as powerful or as well made as the original one,
// and is made for my specific purpose. If you need an argument parser,
// I'd highly recommend you use the flag library already made for go.

package obj

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// @TODO Add a usage output (Is this even needed?)
// @TODO Add better tests
// @TODO Add optional arguments

type Value interface {
	String() string
	Set(string) error
	Get() interface{}
}

var errParse = errors.New("parse error")
var errRange = errors.New("value out of range")

func numError(err error) error {
	ne, ok := err.(*strconv.NumError)
	if !ok {
		return err
	}
	if ne.Err == strconv.ErrSyntax {
		return errParse
	}
	if ne.Err == strconv.ErrRange {
		return errRange
	}
	return err
}

type boolValue bool

func newBoolValue(val interface{}) Value {
	p := new(bool)
	*p = val.(bool)
	return (*boolValue)(p)
}

func (b *boolValue) Set(s string) error {
	v, err := strconv.ParseBool(s)
	if err != nil {
		err = errParse
	}
	*b = boolValue(v)
	return err
}

func (b *boolValue) Get() interface{} { return bool(*b) }

func (b *boolValue) String() string { return strconv.FormatBool(bool(*b)) }

type intValue int

func newIntValue(val interface{}) Value {
	p := new(int)
	*p = val.(int)
	return (*intValue)(p)
}

func (i *intValue) Set(s string) error {
	v, err := strconv.ParseInt(s, 0, strconv.IntSize)
	if err != nil {
		err = numError(err)
	}
	*i = intValue(v)
	return err
}

func (i *intValue) Get() interface{} { return int(*i) }

func (i *intValue) String() string { return strconv.Itoa(int(*i)) }

type int64Value int64

func newInt64Value(val interface{}) Value {
	p := new(int64)
	*p = val.(int64)
	return (*int64Value)(p)
}

func (i *int64Value) Set(s string) error {
	v, err := strconv.ParseInt(s, 0, 64)
	if err != nil {
		err = numError(err)
	}
	*i = int64Value(v)
	return err
}

func (i *int64Value) Get() interface{} { return int64(*i) }

func (i *int64Value) String() string { return strconv.FormatInt(int64(*i), 10) }

type uintValue uint

func newUintValue(val interface{}) Value {
	p := new(uint)
	*p = val.(uint)
	return (*uintValue)(p)
}

func (i *uintValue) Set(s string) error {
	v, err := strconv.ParseUint(s, 0, strconv.IntSize)
	if err != nil {
		err = numError(err)
	}
	*i = uintValue(v)
	return err
}

func (i *uintValue) Get() interface{} { return uint(*i) }

func (i *uintValue) String() string { return strconv.FormatUint(uint64(*i), 10) }

type uint64Value uint64

func newUint64Value(val interface{}) Value {
	p := new(uint64)
	*p = val.(uint64)
	return (*uint64Value)(p)
}

func (i *uint64Value) Set(s string) error {
	v, err := strconv.ParseUint(s, 0, 64)
	if err != nil {
		err = numError(err)
	}
	*i = uint64Value(v)
	return err
}

func (i *uint64Value) Get() interface{} { return uint64(*i) }

func (i *uint64Value) String() string { return strconv.FormatUint(uint64(*i), 10) }

type stringValue string

func newStringValue(val interface{}) Value {
	p := new(string)
	*p = val.(string)
	return (*stringValue)(p)
}

func (s *stringValue) Set(val string) error {
	*s = stringValue(val)
	return nil
}

func (s *stringValue) Get() interface{} { return string(*s) }

func (s *stringValue) String() string { return string(*s) }

type float64Value float64

func newFloat64Value(val interface{}) Value {
	p := new(float64)
	*p = val.(float64)
	return (*float64Value)(p)
}

func (f *float64Value) Set(s string) error {
	v, err := strconv.ParseFloat(s, 64)
	if err != nil {
		err = numError(err)
	}
	*f = float64Value(v)
	return err
}

func (f *float64Value) Get() interface{} { return float64(*f) }

func (f *float64Value) String() string { return strconv.FormatFloat(float64(*f), 'g', -1, 64) }

var acceptedTypes = map[string]func(interface{}) Value{
	"int":     newIntValue,
	"uint":    newUintValue,
	"int64":   newInt64Value,
	"uint64":  newUint64Value,
	"bool":    newBoolValue,
	"float64": newFloat64Value,
	"string":  newStringValue,
}

type MultiValue struct {
	Values []*Flag
	Usage  string
}

func newMultiValue(values []interface{}) *MultiValue {
	mv := new(MultiValue)
	for i := 0; i < len(values); i++ {
		err := mv.addValue(values[i])
		if err != nil {
			panic(err)
		}
	}
	return mv
}

func (mv *MultiValue) addValue(value interface{}) error {
	t := reflect.ValueOf(value).Type().String()
	fn, ok := acceptedTypes[t]
	if !ok {
		return errors.New(fmt.Sprintf("unknown type %s", t))
	}
	v := fn(value)
	flag := newFlag(v, "", "", v.String())
	mv.Values = append(mv.Values, flag)
	return nil
}

type FlagParser struct {
	name        string
	args        []string
	usage       string
	values      map[string]*Flag
	multivalues map[string]*MultiValue
	parsed      bool
}

type Flag struct {
	Name         string
	Usage        string
	Value        Value
	DefaultValue string
}

func (fp *FlagParser) AddType(name string, fn func(interface{}) Value) {
	if _, ok := acceptedTypes[name]; ok {
		panic(fmt.Sprintf("type %s has already been added", name))
	}
	acceptedTypes[name] = fn
}

func NewFlagParser(name string) *FlagParser {
	fp := new(FlagParser)
	fp.name = name
	fp.values = make(map[string]*Flag)
	fp.multivalues = make(map[string]*MultiValue)
	return fp
}

func newFlag(value Value, name string, usage string, defaultValue string) *Flag {
	flag := new(Flag)
	flag.Value = value
	flag.Name = name
	flag.Usage = usage
	flag.DefaultValue = defaultValue
	return flag
}

func (fp *FlagParser) AddArg(name string, defaultValue interface{}, usage string) {
	t := reflect.ValueOf(defaultValue).Type().String()
	fn, ok := acceptedTypes[t]
	if !ok {
		panic(fmt.Sprintf("unknown type %s", t))
	}
	value := fn(defaultValue)
	flag := newFlag(value, name, usage, value.String())
	if _, ok := fp.values[name]; ok {
		panic(fmt.Sprintf("flag redefined: %s", name))
	}
	fp.values[name] = flag
}

func (fp *FlagParser) AddMultiValue(name string, values []interface{}, usage string) {
	mv := newMultiValue(values)
	if _, ok := fp.values[name]; ok {
		panic(fmt.Sprintf("flag redefined: %s", name))
	} else if _, ok := fp.multivalues[name]; ok {
		panic(fmt.Sprintf("flag redefined: %s", name))
	}
	mv.Usage = usage
	fp.multivalues[name] = mv
}

func (fp *FlagParser) parseOne() (bool, error) {
	if len(fp.args) == 0 {
		return false, nil
	}
	s := fp.args[0]
	if len(s) < 2 || s[0] != '-' {
		return false, nil
	}
	if s[1] == '-' {
		return false, nil
	}
	name := s[1:]
	if len(name) == 0 || name[0] == '-' || name[0] == '=' {
		return false, errors.New(fmt.Sprintf("blad flag syntax: %s", s))
	}
	fp.args = fp.args[1:]
	value, ok := fp.values[name]
	if !ok {
		mv, ok := fp.multivalues[name]
		if !ok {
			return false, errors.New(fmt.Sprintf("flag %s not found", name))
		}
		if len(fp.args) < len(mv.Values) {
			return false, errors.New(fmt.Sprintf("flag needs %d arguments: %s", len(mv.Values), name))
		}
		i := 0
		for ; i < len(mv.Values); i++ {
			err := mv.Values[i].Value.Set(fp.args[i])
			if err != nil {
				return false, errors.New(fmt.Sprintf("invalid value %q for flag %s: %v", mv.Values[i], name, err))
			}
		}
		fp.args = fp.args[i:]
	} else {
		if len(fp.args) == 0 {
			return false, errors.New(fmt.Sprintf("flag needs argument: %s", name))
		}
		err := value.Value.Set(fp.args[0])
		if err != nil {
			return false, errors.New(fmt.Sprintf("invalid value %q for flag %s: %v", value, name, err))
		}
		fp.args = fp.args[1:]
	}
	return true, nil
}

func (fp *FlagParser) Parse(args []string) error {
	fp.parsed = true
	fp.args = args
	for {
		seen, err := fp.parseOne()
		if seen {
			continue
		}
		if err == nil {
			break
		}
		return err
	}
	return nil
}

func (fp *FlagParser) Parsed() bool {
	return fp.parsed
}

func (fp *FlagParser) LookUp(name string, loc int) *Flag {
	if _, ok := fp.values[name]; ok {
		return fp.values[name]
	} else if _, ok := fp.multivalues[name]; ok {
		return fp.multivalues[name].Values[loc]
	}
	return nil
}

func (fp *FlagParser) Left() []string {
	return fp.args
}
