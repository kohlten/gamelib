package obj

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
)

func parseBool(word string) bool {
	if word == "on" {
		return true
	} else if word == "off" {
		return false
	} else {
		panic(fmt.Sprintf("value %s is not on or off", word))
	}
}

func parseChannel(word string) TextureChannel {
	switch word {
	case "r":
		return ChannelR
	case "g":
		return ChannelG
	case "b":
		return ChannelB
	case "m":
		return ChannelM
	case "l":
		return ChannelL
	case "z":
		return ChannelZ
	}
	panic(fmt.Sprintf("unknown channel type %s", word))
}

func parseArgs(line []byte, textureMap *TextureMap, commandLine *FlagParser) error {
	args := make([]string, 0)
	reader := bytes.NewReader(line)
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanWords)
	scanner.Scan()
	for scanner.Scan() {
		args = append(args, scanner.Text())
	}
	err := commandLine.Parse(args)
	if err != nil {
		return err
	}
	flag := commandLine.LookUp(commandTextureMapBlendVertical, 0)
	if flag != nil {
		textureMap.BlendVertical = parseBool(flag.Value.Get().(string))
	}
	flag = commandLine.LookUp(commandTextureMapBlendHorizontal, 0)
	if flag != nil {
		textureMap.BlendHorizontal = parseBool(flag.Value.Get().(string))
	}
	flag = commandLine.LookUp(commandTextureMapColorCorrection, 0)
	if flag != nil {
		textureMap.ColorCorrection = parseBool(flag.Value.Get().(string))
	}
	flag = commandLine.LookUp(commandTextureMapClamp, 0)
	if flag != nil {
		textureMap.BlendHorizontal = parseBool(flag.Value.Get().(string))
	}
	flag = commandLine.LookUp(commandTextureMapTextureResolution, 0)
	if flag != nil {
		textureMap.TextureResolution = flag.Value.Get().(uint64)
	}
	flag = commandLine.LookUp(commandTextureMapChannel, 0)
	if flag != nil {
		textureMap.Channel = parseChannel(flag.Value.Get().(string))
	}
	flag = commandLine.LookUp(commandTextureMapBumpMultiplier, 0)
	if flag != nil {
		textureMap.BumpMultiplier = flag.Value.Get().(float64)
	}
	flag = commandLine.LookUp(commandTextureMapReflectionType, 0)
	if flag != nil {
		textureMap.ReflectionType = ReflectionType(flag.Value.Get().(uint))
	}
	flag = commandLine.LookUp(commandTextureMapBoost, 0)
	if flag != nil {
		textureMap.Boost = flag.Value.Get().(float64)
	}
	flag = commandLine.LookUp(commandTextureMapMM, 0)
	if flag != nil {
		for i := 0; i < 2; i++ {
			textureMap.MM = append(textureMap.MM, commandLine.LookUp(commandTextureMapMM, i).Value.Get().(float64))
		}
	}
	flag = commandLine.LookUp(commandTextureMapOriginOffset, 0)
	if flag != nil {
		for i := 0; i < 3; i++ {
			textureMap.OriginOffset = append(textureMap.OriginOffset,
				commandLine.LookUp(commandTextureMapOriginOffset, i).Value.Get().(float64))
		}
	}
	flag = commandLine.LookUp(commandTextureMapScale, 0)
	if flag != nil {
		for i := 0; i < 3; i++ {
			textureMap.Scale = append(textureMap.Scale, commandLine.LookUp(commandTextureMapScale,
				i).Value.Get().(float64))
		}
	}
	flag = commandLine.LookUp(commandTextureMapTurbulence, 0)
	if flag != nil {
		for i := 0; i < 3; i++ {
			textureMap.Turbulence = append(textureMap.Turbulence, commandLine.LookUp(commandTextureMapTurbulence,
				i).Value.Get().(float64))
		}
	}
	left := commandLine.Left()
	if len(left) > 1 {
		return errors.New("too many arguments")
	} else if len(left) == 0 {
		return errors.New("no file at the end of map")
	}
	textureMap.Filename = left[0]
	return nil

}

func parseAmbient(line []byte, texture *Texture) error {
	return parseTextureVec(line, &texture.Ambient, 3)
}

func parseDiffuse(line []byte, texture *Texture) error {
	return parseTextureVec(line, &texture.Diffuse, 3)
}

func parseSpecular(line []byte, texture *Texture) error {
	return parseTextureVec(line, &texture.Specular, 3)
}

func parseTransmissionFilter(line []byte, texture *Texture) error {
	return parseTextureVec(line, &texture.TransmissionFilter, 3)
}

func parseOpticalDensity(line []byte, texture *Texture) error {
	return parseTextureFloat(line, &texture.OpticalDensity)
}

func parseSpecularExponent(line []byte, texture *Texture) error {
	return parseTextureFloat(line, &texture.SpecularExponent)
}

// Need to add a parser for -halo
func parseTransparency(line []byte, texture *Texture) error {
	return parseTextureFloat(line, &texture.Transparency)
}

func parseSharpness(line []byte, texture *Texture) error {
	return parseTextureInt(line, &texture.Sharpness)
}

func parseIllumination(line []byte, texture *Texture) error {
	num := int64(0)
	err := parseTextureInt(line, &num)
	if err != nil {
		return err
	}
	if num > 10 {
		return errors.New("illumination mode is out of bounds")
	}
	texture.Illumination = IlluminationMode(num)
	return nil
}

func parseMapAmbient(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandMapAmbient)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapColorCorrection, "off", "-cc on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapAmbient, commandLine)
}

func parseMapDiffuse(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandMapDiffuse)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapColorCorrection, "off", "-cc on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapAmbient, commandLine)
}

func parseMapSpecular(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandMapSpecular)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapColorCorrection, "off", "-cc on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapSpecular, commandLine)
}

func parseMapSpecularExponent(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandMapSpecularExponent)
	commandLine.AddArg(commandTextureMapBumpMultiplier, 0.0, "-bm mult")
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddArg(commandTextureMapChannel, "l", "-imfchan r | g | b | m | l | z")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapSpecularExponent, commandLine)
}

func parseMapTransparency(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandMapTransparency)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddArg(commandTextureMapChannel, "l", "-imfchan r | g | b | m | l | z")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapTransparency, commandLine)
}

func parseDecal(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandDecal)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddArg(commandTextureMapChannel, "l", "-imfchan r | g | b | m | l | z")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.Decal, commandLine)
}

func parseDisp(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandDisplacement)
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddArg(commandTextureMapChannel, "l", "-imfchan r | g | b | m | l | z")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.Displacement, commandLine)
}

func parseBump(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandBump)
	commandLine.AddArg(commandTextureMapBumpMultiplier, 0.0, "-bm mult")
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddArg(commandTextureMapChannel, "l", "-imfchan r | g | b | m | l | z")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.MapBump, commandLine)
}

func parseReflection(line []byte, texture *Texture) error {
	commandLine := NewFlagParser(commandReflection)
	commandLine.AddArg(commandTextureMapReflectionType, uint(0), "-type type")
	commandLine.AddArg(commandTextureMapBlendHorizontal, "on", "-blendu on | off")
	commandLine.AddArg(commandTextureMapBlendVertical, "on", "-blendv on | off")
	commandLine.AddArg(commandTextureMapColorCorrection, "off", "-cc on | off")
	commandLine.AddArg(commandTextureMapClamp, "off", "-clamp on | off")
	commandLine.AddMultiValue(commandTextureMapMM, []interface{}{float64(0.0), float64(0.0)},
		"-mm base_value gain_value")
	commandLine.AddMultiValue(commandTextureMapOriginOffset, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddMultiValue(commandTextureMapScale, []interface{}{0.0, 0.0, 0.0},
		"-s u v w")
	commandLine.AddMultiValue(commandTextureMapTurbulence, []interface{}{0.0, 0.0, 0.0},
		"-o u v w")
	commandLine.AddArg(commandTextureMapTextureResolution, uint64(0), "-texres resolution")
	return parseArgs(line, &texture.Reflection, commandLine)
}
