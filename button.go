package gamelib

import (
	"github.com/veandco/go-sdl2/sdl"

	"gitlab.com/kohlten/gamelib/vector"
)

type Button struct {
	hoveringSurf      *Texture
	clickedSurf       *Texture
	normalSurf        *Texture
	text              *Text
	position, size    vector.Vec2
	value             string
	hovering, clicked bool
	visible           bool
	active            bool
	actionTimer       *Timer
	drawSize          float32
}

// Creates a new button at the position.
// The position will be adjusted so that the button center will be at the position.
// This will also take into account the drawSize.
func NewButton(text *Text, position, size vector.Vec2, drawSize float32, regular, hover, pressed *Texture) *Button {
	button := new(Button)
	button.position = *position.Sub(size.MulBy(drawSize).DivBy(2.0))
	button.value = ""
	button.text = text
	button.size = *size.MulBy(drawSize)
	button.clickedSurf = pressed
	button.hoveringSurf = hover
	button.normalSurf = regular
	button.actionTimer = NewTimer(200, TimeMilliseconds)
	button.active = true
	button.visible = true
	button.drawSize = drawSize
	return button
}

func (button *Button) SetString(value string) {
	button.value = value
}

func (button *Button) Update() {
	if button.active {
		mousePosX, mousePosY, state := sdl.GetMouseState()
		button.actionTimer.Update()
		if float32(mousePosX) <= button.position.X+button.size.X && float32(mousePosX) >= button.position.X &&
			float32(mousePosY) <= button.position.Y+button.size.Y && float32(mousePosY) >= button.position.Y && !button.actionTimer.IsRunning() {
			if (state&sdl.Button(sdl.BUTTON_LEFT) > 0 || state&sdl.Button(sdl.BUTTON_RIGHT) > 0) && !button.
				clicked {
				button.actionTimer.Start()
				button.clicked = true
			} else if !button.hovering {
				button.hovering = true
			}
		} else {
			button.clicked = false
			button.hovering = false
		}
	}
}

func (button *Button) Draw(angle float64, renderer *sdl.Renderer) error {
	if button.visible {
		center := button.size.Copy().DivBy(2)
		if button.clickedSurf != nil && button.clicked {
			err := button.clickedSurf.Draw(button.position, *center, *button.clickedSurf.Size.Copy().MulBy(button.drawSize), angle, sdl.FLIP_NONE, renderer)
			if err != nil {
				return err
			}
		} else if button.hoveringSurf != nil && button.hovering {
			err := button.hoveringSurf.Draw(button.position, *center, *button.hoveringSurf.Size.Copy().MulBy(button.drawSize), angle, sdl.FLIP_NONE, renderer)
			if err != nil {
				return err
			}
		} else if button.normalSurf != nil {
			err := button.normalSurf.Draw(button.position, *center, *button.normalSurf.Size.Copy().MulBy(button.drawSize), angle, sdl.FLIP_NONE, renderer)
			if err != nil {
				return err
			}
		}
		textSize := button.text.GetSize(button.value, button.drawSize)
		pos := vector.Zero
		pos.X = button.position.X + button.size.X/2
		pos.Y = button.position.Y + button.size.Y/2
		return button.text.Draw(button.value, pos, *textSize.DivBy(2), float32(angle), button.drawSize, nil, renderer)
	}
	return nil
}

func (button *Button) IsPressed() bool {
	return button.clicked
}

// Reset the internal states so you don't have any issues when you stop updating the button
func (button *Button) Reset() {
	button.clicked = false
	button.hovering = false
	button.actionTimer.Reset()
}

func (button *Button) SetClicked(state bool) {
	button.clicked = state
}

func (button *Button) SetVisible(state bool) {
	button.visible = state
}

func (button *Button) SetActive(state bool) {
	button.active = state
}

// Only need to call this if you want to free the sprites
func (button *Button) Free() {
	if button.normalSurf != nil {
		button.normalSurf.Free()
	}
	if button.hoveringSurf != nil {
		button.hoveringSurf.Free()
	}
	if button.clickedSurf != nil {
		button.clickedSurf.Free()
	}
}
