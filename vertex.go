package gamelib

import (
    "github.com/ungerik/go3d/float64/vec3"

    "gitlab.com/kohlten/gamelib/vector"
)

type Vertex struct {
	Position vec3.T
	Normal   vec3.T
	TexCoord vector.Vec2
}

func NewVertex(pos, normal vec3.T, texCoord vector.Vec2) Vertex {
	vertex := Vertex{Position: pos, Normal: normal, TexCoord: texCoord}
	return vertex
}


