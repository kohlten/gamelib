package vector

import (
    "math"
    "math/rand"
)

var Zero = Vec2{}

type Vec2 struct {
    X, Y float32
}

func NewVec2(x, y float32) Vec2 {
    return Vec2{x, y}
}

func NewVec2FromAngle(angle, length float32) Vec2 {
    return NewVec2(length * float32(math.Cos(float64(angle))), length * float32(math.Sin(float64(angle))))
}

func NewVec2Random() Vec2 {
    return NewVec2FromAngle(rand.Float32() * (math.Pi * 2), 1)
}

func (v *Vec2) Copy() *Vec2 {
    return &Vec2{v.X, v.Y}
}

func (v *Vec2) Dot(other *Vec2) float32 {
    return (v.X * other.X) + (v.Y * other.Y)
}

func (v *Vec2) Cross(other *Vec2) float32 {
    return v.X * other.X - v.Y * other.Y
}

func (v *Vec2) AngleBetween(other *Vec2) float32 {
    return float32(math.Atan2(float64(v.Cross(other)), float64(v.Dot(other))))
}

func (v *Vec2) MagSqrt() float32 {
    return (v.X * v.X) + (v.Y * v.Y)
}

func (v *Vec2) Mag() float32 {
    return float32(math.Sqrt(float64(v.MagSqrt())))
}

func (v *Vec2) Dist(other *Vec2) float32 {
    return float32(math.Sqrt(math.Pow(float64(v.X - other.X), 2.0) + math.Pow(float64(v.Y - other.Y), 2.0)))
}

func (v *Vec2) Heading() float32 {
    return float32(math.Atan2(float64(v.Y), float64(v.X)))
}

// This section returns a *Vec2 so you can chain commands together
// E.G v.Add(o).Sub(a).Mul(b).DivBy(2)

func (v *Vec2) Add(other *Vec2) *Vec2 {
    v.X += other.X
    v.Y += other.Y
    return v
}

func (v *Vec2) Sub(other *Vec2) *Vec2 {
    v.X -= other.X
    v.Y -= other.Y
    return v
}

func (v *Vec2) Mul(other *Vec2) *Vec2 {
    v.X *= other.X
    v.Y *= other.Y
    return v
}

func (v *Vec2) Div(other *Vec2) *Vec2 {
    v.X /= other.X
    v.Y /= other.Y
    return v
}

func (v *Vec2) AddBy(val float32) *Vec2 {
    v.X += val
    v.Y += val
    return v
}

func (v *Vec2) SubBy(val float32) *Vec2 {
    v.X -= val
    v.Y -= val
    return v
}

func (v *Vec2) MulBy(val float32) *Vec2 {
    v.X *= val
    v.Y *= val
    return v
}

func (v *Vec2) DivBy(val float32) *Vec2 {
    v.X /= val
    v.Y /= val
    return v
}

func (v *Vec2) Norm() *Vec2 {
    vLen := v.Mag()
    if vLen != 0 {
        v.MulBy(1 / vLen)
    }
    return v
}

func (v *Vec2) SetMag(val float32) *Vec2 {
    v.Norm()
    v.MulBy(val)
    return v
}

func (v *Vec2) Rotate(origin *Vec2, angle float32) *Vec2 {
    v.Sub(origin)
    newV := Vec2{}
    newV.X = v.X * float32(math.Cos(float64(angle))) - v.Y * float32(math.Sin(float64(angle)))
    newV.Y = v.X * float32(math.Sin(float64(angle))) + v.Y * float32(math.Cos(float64(angle)))
    newV.Add(origin)
    v.Set(&newV)
    return v
}

func (v *Vec2) Set(newV *Vec2) *Vec2 {
    v.X = newV.X
    v.Y = newV.Y
    return v
}

func (v *Vec2) Equal(other *Vec2) bool {
    return v.X == other.X && v.Y == other.Y
}

func (v *Vec2) NotEqual(other *Vec2) bool {
    return !v.Equal(other)
}

func Constrain(n, low, high float32) float32 {
    return float32(math.Max(math.Min(float64(n), float64(high)), float64(low)))
}

func Map(n float32, r1, r2 Vec2) float32 {
    return (n - r1.X) / (r1.Y - r1.X) * (r2.Y - r2.X) + r2.X
}

func MapConstrain(n float32, r1, r2 Vec2) float32 {
    val := Map(n, r1, r2)
    if r2.X < r2.Y {
        return Constrain(val, r2.X, r2.Y)
    }
    return Constrain(val, r2.Y, r2.X)
}






