package gamelib

import (
    "encoding/json"
    "errors"
    "fmt"
    "io/ioutil"
    "path/filepath"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"
)

type Text struct {
	characters map[string]*Texture
	size       vector.Vec2
}

func (text *Text) addSurfaces(surf *Surface, order string, renderer *sdl.Renderer) error {
	splitCharacters, err := SplitSpriteSheet(surf, text.size)
	if err != nil {
		return err
	}
	if len(splitCharacters) != len(order) {
		return errors.New("not enough sprites for the order")
	}
	for i := 0; i < len(splitCharacters); i++ {
		key := string(order[i])
		tex, err := NewTextureFromSurface(splitCharacters[i], renderer)
		if err != nil {
			return err
		}
		splitCharacters[i].Free()
		text.characters[key] = tex
	}
	return nil
}

func NewTextFromFile(size vector.Vec2, filename, order string, renderer *sdl.Renderer) (*Text, error) {
	text := new(Text)
	text.size = size
	text.characters = make(map[string]*Texture)
	spriteSheet, err := NewSurfaceFromFile(filename)
	if err != nil {
		return nil, err
	}
	err = text.addSurfaces(spriteSheet, order, renderer)
	if err != nil {
		return nil, err
	}
	return text, nil
}

func NewTextFromSurface(size vector.Vec2, spriteSheet *Surface, order string, renderer *sdl.Renderer) (*Text, error) {
	text := new(Text)
	text.size = size
	text.characters = make(map[string]*Texture)
	err := text.addSurfaces(spriteSheet, order, renderer)
	if err != nil {
		return nil, err
	}
	return text, nil
}

func NewTextFromJson(filename string, renderer *sdl.Renderer) (*Text, error) {
	jsonText, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	jsonMap := make(map[string]interface{})
	err = json.Unmarshal(jsonText, &jsonMap)
	if err != nil {
		return nil, err
	}
	file, ok := jsonMap["file"]
	if !ok {
		return nil, errors.New("json is not valid")
	}
	size, ok := jsonMap["size"]
	if !ok {
		return nil, errors.New("json is not valid")
	}
	order, ok := jsonMap["order"]
	if !ok {
		return nil, errors.New("json is not valid")
	}
	x := size.([]interface{})[0].(float64)
	y := size.([]interface{})[1].(float64)
	directory, _ := filepath.Split(filename)
	return NewTextFromFile(vector.NewVec2(float32(x), float32(y)), directory + file.(string), order.(string), renderer)
}

func (text *Text) GetSize(value string, drawSize float32) vector.Vec2 {
	size := vector.Zero
	for i := 0; i < len(value); i++ {
		if value[i] == '\n' {
			size.Y += text.size.Y * drawSize
		} else {
			size.X += text.size.X * drawSize
		}
	}
	if len(value) > 0 {
		size.Y += text.size.Y * drawSize
	}
	return size
}

// Zoom here is a scaling factor. A zoom of zero will result in a single pixel. A zoom of 1 will render the original size.
// Smoothing will do software AA on the surface. This is slower but the image will look better.
// The offsets here are for effects that you want to do to the text. If you don't want to do that, just pass nil.
// DrawSize affects how large the text will be. A value of 0.5 will make it half the size. A value of 1.0 will make it the same size.
// A value of 2.0 will make it double the size.
func (text *Text) Draw(value string, position, center vector.Vec2, angle, drawSize float32, offsets *[]vector.Vec2, renderer *sdl.Renderer) error {
	initialPos := position
	if offsets != nil {
		if len(*offsets) < len(value) {
			return errors.New("not enough offsets to draw text")
		}
	}
	for i := 0; i < len(value); i++ {
		if value[i] == '\n' {
			position.X = initialPos.X
			position.Y += text.size.Y * drawSize
			continue
		} else if value[i] == ' ' {
			position.X += text.size.X * drawSize
			continue
		}
		val, ok := text.characters[string(value[i])]
		if ok != true {
			return errors.New(fmt.Sprint("value", value[i], "was not found in the table"))
		}
		current := position
		if offsets != nil {
			current.Add(&(*offsets)[i])
		}
		err := val.Draw(position, center, vector.NewVec2(text.size.X * drawSize, text.size.Y * drawSize), float64(angle), sdl.FLIP_NONE, renderer)
		if err != nil {
			return err
		}
		position.X += text.size.X * drawSize
	}
	return nil
}

func (text *Text) GetOrder() string {
	keys := ""
	for key := range text.characters {
		keys += key
	}
	return keys
}

func (text *Text) GetLetter(letter byte) *Texture {
	return text.characters[string(letter)]
}

func (text *Text) SetLetter(letter byte, surf *Surface, renderer *sdl.Renderer) error {
	tex, err := NewTextureFromSurface(surf, renderer)
	if err != nil {
		return err
	}
	text.characters[string(letter)] = tex
	return nil
}

func (text *Text) Free() {
	for _, value := range text.characters {
		value.Free()
	}
}
