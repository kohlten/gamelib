package gamelib

import (
	"errors"
	"fmt"
	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"path"
	"unsafe"

	"gitlab.com/kohlten/gamelib/vector"
)

var SupportedFormats = map[string]int{"tga": 0, "bmp": 0, "pnm": 0, "pbm": 0, "pgm": 0, "ppm": 0, "xpm": 0, "xcf": 0, "pcx": 0,
	"gif": 0, "jpg": 0, "jpeg": 0, "tif": 0, "tiff": 0, "lbm": 0, "iff": 0, "png": 0}

const (
	FileTypeOk       = 0
	FileTypeUnknown  = 1
	FileTypeNotFound = 2
)

type Surface struct {
	surf *sdl.Surface
	Size vector.Vec2
}

type mask struct {
	r uint32
	g uint32
	b uint32
	a uint32
}

// Create a new sdl.PixelFormat using default values.
// These default values are for 32 bit images with alpha.
// If you do not need alpha, simply pass nil to NewBlankSurface.
//
// This value needs to be freed with format.Free.
func GetDefaultPixelFormat() (*sdl.PixelFormat, error) {
	pixelEnum := sdl.MasksToPixelFormatEnum(32, 255, 65280, 16711680, 4278190080)
	format, err := sdl.AllocFormat(pixelEnum)
	if err != nil {
		return nil, err
	}
	return format, nil
}

func NewSurfaceFromFile(name string) (*Surface, error) {
	fileType := IsValidFile(name)
	if fileType == FileTypeNotFound {
		fmt.Println("Gamelib Warning: ", name, "is not supported, going to try to load anyways")
	} else if fileType == FileTypeUnknown {
		fmt.Println("Gamelib Warning: ", name, " type was not found, going to try to load anyways")
	}
	surf := new(Surface)
	imgSurface, err := img.Load(name)
	if err != nil {
		return nil, err
	}
	surf.surf, err = imgSurface.Convert(imgSurface.Format, 0)
	if err != nil {
		return nil, err
	}
	imgSurface.Free()
	surf.updateSize()
	return surf, nil
}

func getMasks() mask {
	masks := mask{}
	if sdl.BYTEORDER == sdl.BIG_ENDIAN {
		masks.r = 0xff000000
		masks.g = 0x00ff0000
		masks.b = 0x0000ff00
		masks.a = 0x000000ff
	} else {
		masks.r = 0x000000ff
		masks.g = 0x0000ff00
		masks.b = 0x00ff0000
		masks.a = 0xff000000
	}
	return masks
}

// The format matters a lot here. If the format of the image is not 16 bpp, it will create a 8 bpp image which has no alpha.
func NewBlankSurface(size vector.Vec2, color sdl.Color, format *sdl.PixelFormat) (*Surface, error) {
	surface := new(Surface)
	masks := getMasks()
	tmp, err := sdl.CreateRGBSurface(0, int32(size.X), int32(size.Y), 32, masks.r, masks.g, masks.b, masks.a)
	if err != nil {
		return nil, err
	}
	if format != nil {
		surface.surf, err = tmp.Convert(format, 0)
		if err != nil {
			return nil, err
		}
	} else {
		surface.surf, err = tmp.Convert(tmp.Format, 0)
		if err != nil {
			return nil, err
		}
	}
	err = surface.Wipe(color)
	if err != nil {
		return nil, err
	}
	tmp.Free()
	surface.updateSize()
	return surface, nil
}

func (surf *Surface) Wipe(color sdl.Color) error {
	colorInt := sdl.MapRGBA(surf.surf.Format, color.R, color.G, color.B, color.A)
	err := surf.surf.FillRect(nil, colorInt)
	return err
}

func (surf *Surface) SetAlpha(alpha uint8) error {
	return surf.surf.SetAlphaMod(alpha)
}

func (surf *Surface) Resize(size vector.Vec2) error {
	scale := size.Copy().Div(&surf.Size)
	newSurf := gfx.RotoZoomSurfaceXY(surf.surf, 0.0, float64(scale.X), float64(scale.Y), 0)
	if newSurf == nil {
		return errors.New("failed to resize")
	}
	surf.surf.Free()
	surf.surf = newSurf
	surf.updateSize()
	return nil
}

func (surf *Surface) GetSDLSurf() *sdl.Surface {
	return surf.surf
}

func (surf *Surface) GetPixels() []byte {
	return surf.surf.Pixels()
}

func (surf *Surface) GetRawPixels() unsafe.Pointer {
	return surf.surf.Data()
}

func (surf *Surface) updateSize() {
	surf.Size = vector.NewVec2(float32(surf.surf.W), float32(surf.surf.H))
}

func (surf *Surface) GetFormat() *sdl.PixelFormat {
	return surf.surf.Format
}

func (surf *Surface) GetPixel(position vector.Vec2) (int32, error) {
	if position.X < 0 || position.Y < 0 || int32(position.X) > surf.surf.W || int32(position.Y) > surf.surf.H {
		return 0, errors.New("position greater than the size or less than zero")
	}
	bpp := surf.surf.Format.BitsPerPixel
	pixels := surf.surf.Pixels()
	location := int32(position.Y)*surf.surf.Pitch + int32(position.X)*int32(bpp)
	switch bpp {
	case 1:
		return int32(pixels[location]), nil
	case 2:
		return int32(pixels[location] | pixels[location+1]<<8), nil
	case 3:
		return int32(pixels[location] | pixels[location+1]<<8 | pixels[location+2]<<16), nil
	case 4:
		return int32(pixels[location] | pixels[location+1]<<8 | pixels[location+2]<<16 | pixels[location+3]<<32), nil
	default:
		return 0, errors.New("bytes per pixel not supported")
	}
}

func (surf *Surface) SetPixel(position vector.Vec2, color sdl.Color) error {
	if position.X < 0 || position.Y < 0 || int32(position.X) > surf.surf.W || int32(position.Y) > surf.surf.H {
		return errors.New("position greater than the size or less than zero")
	}
	if surf.surf.MustLock() {
		err := surf.surf.Lock()
		if err != nil {
			return err
		}
	}
	bpp := surf.surf.Format.BytesPerPixel
	pixels := surf.surf.Pixels()
	location := int32(position.Y)*surf.surf.Pitch + int32(position.X)*int32(bpp)
	intColor := sdl.MapRGBA(surf.surf.Format, color.R, color.G, color.B, color.A)
	switch bpp {
	case 1:
		pixels[location] = byte(intColor)
	case 2:
		pixels[location] = byte(intColor & 0xff)
		pixels[location+1] = byte((intColor >> 8) & 0xff)
	case 3:
		pixels[location] = byte(intColor & 0xff)
		pixels[location+1] = byte((intColor >> 8) & 0xff)
		pixels[location+2] = byte((intColor >> 16) & 0xff)
	case 4:
		pixels[location] = byte(intColor & 0xff)
		pixels[location+1] = byte((intColor >> 8) & 0xff)
		pixels[location+2] = byte((intColor >> 16) & 0xff)
		pixels[location+3] = byte((intColor >> 24) & 0xff)
	}
	if surf.surf.MustLock() {
		surf.surf.Unlock()
	}
	return nil
}

func (surf *Surface) Draw(renderer *sdl.Renderer, position vector.Vec2) error {
	texture, err := renderer.CreateTextureFromSurface(surf.surf)
	if err != nil {
		return err
	}
	rect := sdl.Rect{X: int32(position.X), Y: int32(position.Y), W: surf.surf.W, H: surf.surf.H}
	err = renderer.Copy(texture, nil, &rect)
	if err != nil {
		return err
	}
	err = texture.Destroy()
	if err != nil {
		return err
	}
	return nil
}

func (surf *Surface) Blit(srcRect *sdl.Rect, dst *Surface, dstRect *sdl.Rect) error {
	return surf.surf.Blit(srcRect, dst.surf, dstRect)
}

func (surf *Surface) BlitTo(srcRect *sdl.Rect, dst *Surface, dstRect *sdl.Rect) error {
	return dst.surf.Blit(srcRect, surf.surf, dstRect)
}

func (surf *Surface) GetSDLSurface() *sdl.Surface {
	return surf.surf
}

// Rotates the surface and updates the internal surface to that value
func (surf *Surface) Rotate(angle, zoom float64, smooth bool) {
	smoothInt := 0
	if smooth {
		smoothInt = 1
	}
	rotated := gfx.RotoZoomSurface(surf.surf, angle, zoom, smoothInt)
	surf.surf.Free()
	surf.surf = rotated
	surf.updateSize()
}

func (surf *Surface) ZoomTmp(zoom float64, smooth bool) *Surface {
	smoothInt := 0
	if smooth {
		smoothInt = 1
	}
	zoomed := gfx.ZoomSurface(surf.surf, zoom, zoom, smoothInt)
	newSurf := new(Surface)
	newSurf.surf = zoomed
	newSurf.updateSize()
	return newSurf
}

func (surf *Surface) Crop(pos, size vector.Vec2) error {
	newSurf, err := surf.CropTmp(pos, size)
	if err != nil {
		return err
	}
	surf.surf.Free()
	surf.surf = newSurf.surf
	surf.updateSize()
	return nil
}

func (surf *Surface) CropTmp(pos, size vector.Vec2) (*Surface, error) {
	newSurf, err := NewBlankSurface(size, sdl.Color{0, 0, 0, 0}, surf.GetFormat())
	if err != nil {
		return nil, err
	}
	rect := sdl.Rect{
		X: int32(pos.X),
		Y: int32(pos.Y),
		W: int32(size.X),
		H: int32(size.Y),
	}
	err = newSurf.BlitTo(&rect, surf, nil)
	if err != nil {
		return nil, err
	}
	return newSurf, nil
}

// Rotates the surface and returns the rotated version of the image. Does not change the internal surface.
func (surf *Surface) RotateTmp(angle, zoom float64, smooth bool) *Surface {
	smoothInt := 0
	if smooth {
		smoothInt = 1
	}
	rotated := gfx.RotoZoomSurface(surf.surf, angle, zoom, smoothInt)
	outputSurf := new(Surface)
	outputSurf.surf = rotated
	outputSurf.updateSize()
	return outputSurf
}

// Zoom here is a scaling factor. A zoom of zero will result in a single pixel. A zoom of 1 will render the original size.
// Smoothing will do software AA on the surface. This is slower but the image will look better.
func (surf *Surface) RotateAndDraw(angle, zoom float64, smooth bool, pos vector.Vec2, renderer *sdl.Renderer) error {
	smoothInt := 0
	if smooth {
		smoothInt = 1
	}
	rotated := gfx.RotoZoomSurface(surf.surf, angle, zoom, smoothInt)
	rotatedSurf := new(Surface)
	rotatedSurf.surf = rotated
	err := rotatedSurf.Draw(renderer, pos)
	rotatedSurf.Free()
	return err
}

func (surf *Surface) RotateAndDrawToSurface(angle, zoom float64, smooth bool, pos vector.Vec2, surface *Surface) error {
	smoothInt := 0
	if smooth {
		smoothInt = 1
	}
	rotated := gfx.RotoZoomSurface(surf.surf, angle, zoom, smoothInt)
	rotatedSurf := new(Surface)
	rotatedSurf.surf = rotated
	err := rotatedSurf.Blit(nil, surface, &sdl.Rect{X: int32(pos.X), Y: int32(pos.Y), W: int32(surface.Size.X), H: int32(surface.Size.Y)})
	return err
}

func (surf *Surface) Free() {
	surf.surf.Free()
}

// Splits a sprite sheet by the frame size. It will split by row.
func SplitSpriteSheet(spriteSheet *Surface, frameSize vector.Vec2) ([]*Surface, error) {
	sprites := make([]*Surface, 0)
	spriteSheetSize := spriteSheet.Size
	for i := 0; i < int(spriteSheetSize.Y); i += int(frameSize.Y) {
		for j := 0; j < int(spriteSheetSize.X); j += int(frameSize.X) {
			sprite, err := NewBlankSurface(frameSize, sdl.Color{}, spriteSheet.GetFormat())
			if err != nil {
				return nil, err
			}
			err = spriteSheet.Blit(&sdl.Rect{X: int32(j), Y: int32(i), W: int32(frameSize.X), H: int32(frameSize.Y)}, sprite, nil)
			if err != nil {
				return nil, err
			}
			sprites = append(sprites, sprite)
		}
	}
	spriteSheet.Free()
	return sprites, nil
}

func CombineSpriteSheet(imgs []*Surface, frameSize vector.Vec2, rows uint) (*Surface, error) {
	if len(imgs) == 0 {
		return nil, fmt.Errorf("not enough images")
	}
	if rows == 0 {
		rows = 1
	}
	imgsPerRow := uint(len(imgs)) / rows
	rowSize := float32(imgsPerRow) * frameSize.X
	colSize := float32(rows) * frameSize.Y
	output, err := NewBlankSurface(vector.NewVec2(rowSize, colSize), sdl.Color{}, imgs[0].GetFormat())
	if err != nil {
		return nil, err
	}
	imgLoc := 0
	for i := float32(0); i < colSize; i += frameSize.Y {
		for j := float32(0); j < rowSize; j += frameSize.X {
			if imgLoc >= len(imgs) {
				break
			}
			surf := imgs[imgLoc]
			imgLoc++
			err = surf.Blit(nil, output, &sdl.Rect{X: int32(j), Y: int32(i), W: int32(frameSize.X), H: int32(frameSize.Y)})
			if err != nil {
				return nil, err
			}
		}
	}
	return output, nil
}

func NewSurfaceFromImageMemory(data []byte) (*Surface, error) {
	ops, err := sdl.RWFromMem(data)
	if err != nil {
		return nil, err
	}
	surf, err := img.LoadRW(ops, false)
	err = ops.Close()
	if err != nil {
		return nil, err
	}
	convertedSurf, err := surf.Convert(surf.Format, 0)
	surf.Free()
	surface := new(Surface)
	surface.surf = convertedSurf
	surface.updateSize()
	return surface, nil
}

func NewSurfaceFromMemory(data []byte, width, height uint) (*Surface, error) {
	surf, err := sdl.CreateRGBSurfaceFrom(unsafe.Pointer(&data[0]), int32(width), int32(height), 32, 2304, 0, 0, 0, 0)
	if err != nil {
		return nil, err
	}
	goSurf := Surface{surf: surf}
	goSurf.updateSize()
	return &goSurf, nil
}

func IsValidFile(filename string) uint {
	extension := path.Ext(filename)[1:]
	if extension == "" {
		return FileTypeUnknown
	}
	_, ok := SupportedFormats[extension]
	if !ok {
		return FileTypeNotFound
	}
	return FileTypeOk
}
