package gamelib

const (
	TimerInfinite = 0

	TimeNanoseconds = 0
	TimeMilliseconds = 1
	TimeSeconds = 2
	TimeNone = 3
)

type Timer struct {
	/* Time that the timer was started */
	start uint64
	/* Target time */
	target    uint64
	started   bool
	completed bool
}

func NewTimer(target uint64, timeType int) *Timer {
	timer := new(Timer)
	timer.SetTargetTime(target, timeType)
	return timer
}

func (timer *Timer) SetTargetTime(target uint64, timeType int) {
	if timeType == TimeNanoseconds {
		timer.target = target
	} else if timeType == TimeMilliseconds {
		timer.target = target * 1e+6
	} else if timeType == TimeSeconds {
		timer.target = target * 1e+9
	}
}

func (timer *Timer) Start() {
	timer.started = true
	timer.completed = false
	timer.start = GetTimeNS()
}

func (timer *Timer) Stop() {
	timer.started = false
}

func (timer *Timer) Reset() {
	timer.Stop()
	timer.Start()
	timer.completed = false
}

func (timer *Timer) GetElapsedTime(timeType int) uint64 {
	current := GetTimeNS()
	if timeType == TimeNanoseconds {
		return current - timer.start
	} else if timeType == TimeMilliseconds {
		return uint64(float64(current - timer.start) * 1e-6)
	} else if timeType == TimeSeconds {
		return uint64(float64(current - timer.start) * 1e-9)
	}
	return current - timer.start
}

func (timer *Timer) IsCompleted() bool {
	return timer.completed
}

func (timer *Timer) IsRunning() bool {
	return timer.started
}

func (timer *Timer) Update() {
	if timer.started {
		if timer.target > 0 && GetTimeNS() - timer.start >= timer.target {
			timer.started = false
			timer.completed = true
		}
	}
}
