package gamelib

import (
    "math"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"
)

type Drawable interface {
    SetPixel(position vector.Vec2, color sdl.Color) error
}

func drawPixels(surf Drawable, center, pos vector.Vec2, color sdl.Color) {
    _ = surf.SetPixel(vector.NewVec2(center.X + pos.X, center.Y + pos.Y), color)
    _ = surf.SetPixel(vector.NewVec2(center.X - pos.X, center.Y + pos.Y), color)
    _ = surf.SetPixel(vector.NewVec2(center.X + pos.X, center.Y - pos.Y), color)
    _ = surf.SetPixel(vector.NewVec2(center.X - pos.X, center.Y - pos.Y), color)
    _ = surf.SetPixel(vector.NewVec2(center.X + pos.Y, center.Y + pos.X), color)
    _ = surf.SetPixel(vector.NewVec2(center.X - pos.Y, center.Y + pos.X), color)
    _ = surf.SetPixel(vector.NewVec2(center.X + pos.Y, center.Y - pos.X), color)
    _ = surf.SetPixel(vector.NewVec2(center.X - pos.Y, center.Y - pos.X), color)
}

func drawFilled(surf Drawable, center, pos vector.Vec2, color sdl.Color) {
    DrawLine(surf, vector.NewVec2(center.X + pos.X, center.Y + pos.Y), vector.NewVec2(center.X - pos.X, center.Y + pos.Y), color)
    DrawLine(surf, vector.NewVec2(center.X + pos.X, center.Y - pos.Y), vector.NewVec2(center.X - pos.X, center.Y - pos.Y), color)
    DrawLine(surf, vector.NewVec2(center.X + pos.Y, center.Y + pos.X), vector.NewVec2(center.X - pos.Y, center.Y + pos.X), color)
    DrawLine(surf, vector.NewVec2(center.X + pos.Y, center.Y - pos.X), vector.NewVec2(center.X - pos.Y, center.Y - pos.X), color)
}

func DrawCircle(surf Drawable, pos vector.Vec2, radius uint32, color sdl.Color, filled bool) {
    other := vector.Vec2{0, float32(radius)}
    d := float32(3 - 2*int32(radius))
    if !filled {
        drawPixels(surf, pos, other, color)
    } else {
        drawFilled(surf, pos, other, color)
    }
    for other.Y >= other.X {
        other.X += 1
        if d > 0 {
            other.Y -= 1
            d = d + 4*(other.X-other.Y) + 10
        } else {
            d = d + 4*other.X + 6
        }
        if !filled {
            drawPixels(surf, pos, other, color)
        } else {
            drawFilled(surf, pos, other, color)
        }
    }
}

func drawLineL(surf Drawable, pos1, pos2 vector.Vec2, color sdl.Color) {
    d := vector.NewVec2(pos2.X - pos1.X, pos2.Y - pos1.Y)
    yi := float32(1)
    if d.Y < 0 {
        yi = float32(-1)
        d.Y = -d.Y
    }
    D := 2.0*d.Y - d.X
    y := pos1.Y
    for x := pos1.X; x < pos2.X; x++ {
        _ = surf.SetPixel(vector.Vec2{x, y}, color)
        if D > 0 {
            y += yi
            D -= 2 * d.X
        }
        D += 2 * d.Y
    }
}

func drawLineH(surf Drawable, pos1, pos2 vector.Vec2, color sdl.Color) {
    d := vector.NewVec2(pos2.X - pos1.X, pos2.Y - pos1.Y)
    xi := float32(1)
    if d.X < 0 {
        xi = float32(-1)
        d.X = -d.X
    }
    D := 2.0*d.X - d.Y
    x := pos1.X
    for y := pos1.Y; y < pos2.Y; y++ {
        _ = surf.SetPixel(vector.Vec2{x, y}, color)
        if D > 0 {
            x += xi
            D -= 2 * d.Y
        }
        D += 2 * d.X
    }
}

func DrawLine(surf Drawable, pos1, pos2 vector.Vec2, color sdl.Color) {
    if math.Abs(float64(pos2.Y-pos1.Y)) < math.Abs(float64(pos2.X-pos1.X)) {
        if pos1.X > pos2.X {
            drawLineL(surf, pos2, pos1, color)
        } else {
            drawLineL(surf, pos1, pos2, color)
        }
    } else {
        if pos1.Y > pos2.Y {
            drawLineH(surf, pos2, pos1, color)
        } else {
            drawLineH(surf, pos1, pos2, color)
        }
    }
}

func DrawRect(surf Drawable, rect sdl.Rect, color sdl.Color, filled bool) {
    if filled {
        for x := rect.X; x < rect.X+rect.W; x++ {
            DrawLine(surf, vector.NewVec2(float32(x), float32(rect.Y)), vector.NewVec2(float32(x), float32(rect.Y + rect.H)), color)
        }
    } else {
        DrawLine(surf, vector.NewVec2(float32(rect.X), float32(rect.Y)), vector.NewVec2(float32(rect.X + rect.W), float32(rect.Y)), color)
        DrawLine(surf, vector.NewVec2(float32(rect.X + rect.W), float32(rect.Y)), vector.NewVec2(float32(rect.X + rect.W), float32(rect.Y + rect.H)), color)
        DrawLine(surf, vector.NewVec2(float32(rect.X + rect.W), float32(rect.Y + rect.H)), vector.NewVec2(float32(rect.X), float32(rect.Y + rect.H)), color)
        DrawLine(surf, vector.NewVec2(float32(rect.X), float32(rect.Y + rect.H)), vector.NewVec2(float32(rect.X), float32(rect.Y)), color)
    }
}
