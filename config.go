package gamelib

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	ConfigBool   = 0
	ConfigNum    = 1
	ConfigVector = 2
	ConfigString = 3
)

type configNode struct {
	nodeType int
	value    interface{}
	key      string
}

type Config struct {
	table    map[string]*configNode
	filename string
}

type configNodeSlice []*configNode

func isBool(data []byte) bool {
	return string(data) == "TRUE" || string(data) == "FALSE"
}

func isNum(data []byte) bool {
	dots := 0
	for i := 0; i < len(data); i++ {
		if data[i] < '0' || data[i] > '9' {
			if data[i] == '.' {
				dots++
			}
			if data[i] != '-' && data[i] != '+' && data[i] != '.' {
				return false
			}
		}
	}
	if dots > 1 {
		return false
	}
	return true
}

func isVector(data []byte) bool {
	split := bytes.Split(data, []byte(","))
	if len(split) <= 1 {
		return false
	}
	for i := 0; i < len(split); i++ {
		if !isNum(split[i]) {
			return false
		}
	}
	return true
}

func (config *Config) addBool(data [][]byte) {
	node := new(configNode)
	node.nodeType = ConfigBool
	node.key = string(data[0])
	if string(data[1]) == "TRUE" {
		node.value = true
	} else {
		node.value = false
	}
	config.table[string(data[0])] = node
}

func (config *Config) addNum(data [][]byte) {
	node := new(configNode)
	node.nodeType = ConfigNum
	node.value, _ = strconv.ParseFloat(string(data[1]), 64)
	node.key = string(data[0])
	config.table[string(data[0])] = node
}

func (config *Config) addVector(data [][]byte) {
	node := new(configNode)
	node.nodeType = ConfigVector
	values := bytes.Split(data[1], []byte(","))
	vec := make([]float64, len(values))
	for i := 0; i < len(values); i++ {
		val, _ := strconv.ParseFloat(string(values[i]), 64)
		vec[i] = val
	}
	node.value = vec
	node.key = string(data[0])
	config.table[string(data[0])] = node
}

func (config *Config) addString(data [][]byte) {
	node := new(configNode)
	node.nodeType = ConfigString
	node.value = string(data[1])
	node.key = string(data[0])
	config.table[string(data[0])] = node
}

func (config *Config) parseLines(data []byte) error {
	lines := bytes.Split(data, []byte("\n"))
	for i := 0; i < len(lines); i++ {
		if len(lines[i]) == 0 {
			continue
		}
		if bytes.Trim(lines[i], " \t\n\v")[0] == '#' {
			continue
		}
		values := bytes.Split(lines[i], []byte(" "))
		if len(values) < 2 {
			continue
		}
		if len(values) > 2 {
			return errors.New(fmt.Sprint("too many arguments on line ", i))
		}
		if isBool(values[1]) {
			config.addBool(values)
		} else if isNum(values[1]) {
			config.addNum(values)
		} else if isVector(values[1]) {
			config.addVector(values)
		} else {
			config.addString(values)
		}
	}
	return nil
}

func ParseConfig(filename string) (*Config, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	config := new(Config)
	config.table = make(map[string]*configNode)
	err = config.parseLines(data)
	if err != nil {
		return nil, err
	}
	config.filename = filename
	return config, nil
}

func ParseConfigString(value string) (*Config, error) {
	config := new(Config)
	config.table = make(map[string]*configNode)
	err := config.parseLines([]byte(value))
	if err != nil {
		return nil, err
	}
	return config, nil
}

func NewBlankConfig() *Config {
	config := new(Config)
	config.table = make(map[string]*configNode)
	return config
}

func (config *Config) GetDataBool(key string) (bool, error) {
	value, in := config.table[key]
	if !in {
		return false, errors.New(fmt.Sprint("failed to find key", key))
	}
	if value.nodeType != ConfigBool {
		return false, errors.New("key is not a bool")
	}
	return value.value.(bool), nil
}

func (config *Config) GetDataNum(key string) (float64, error) {
	value, in := config.table[key]
	if !in {
		return 0.0, errors.New(fmt.Sprint("failed to find key", key))
	}
	if value.nodeType != ConfigNum {
		return 0.0, errors.New("key is not a num")
	}
	return value.value.(float64), nil
}

func (config *Config) GetDataVector(key string) ([]float64, error) {
	value, in := config.table[key]
	if !in {
		return nil, errors.New(fmt.Sprint("failed to find key", key))
	}
	if value.nodeType != ConfigVector {
		return nil, errors.New("key is not a num")
	}
	return value.value.([]float64), nil
}

func (config *Config) GetDataString(key string) (string, error) {
	value, in := config.table[key]
	if !in {
		return "", errors.New(fmt.Sprint("failed to find key", key))
	}
	if value.nodeType != ConfigString {
		return "", errors.New("key is not a num")
	}
	return value.value.(string), nil
}

func (config *Config) GetData(key string) (interface{}, int, error) {
	value, in := config.table[key]
	if !in {
		return nil, 0, errors.New(fmt.Sprint("failed to find key", key))
	}
	return value.value, value.nodeType, nil
}

func newConfigNode(nodeType int, data interface{}, key string) *configNode {
	node := new(configNode)
	node.nodeType = nodeType
	node.value = data
	node.key = key
	return node
}

func (config *Config) SetDataBool(key string, data bool) error {
	value, in := config.table[key]
	if !in {
		value = newConfigNode(ConfigBool, data, key)
	} else {
		if value.nodeType != ConfigBool {
			return errors.New("key is not a bool")
		}
		value.value = data
	}
	config.table[key] = value
	return nil
}

func (config *Config) SetDataNum(key string, data float64) error {
	value, in := config.table[key]
	if !in {
		value = newConfigNode(ConfigNum, data, key)
	} else {
		if value.nodeType != ConfigNum {
			return errors.New("key is not a num")
		}
		value.value = data
	}
	config.table[key] = value
	return nil
}

func (config *Config) SetDataVector(key string, data []float64) error {
	value, in := config.table[key]
	if !in {
		value = newConfigNode(ConfigVector, data, key)
	} else {
		if value.nodeType != ConfigVector {
			return errors.New("key is not a vector")
		}
		value.value = data
	}
	config.table[key] = value
	return nil
}

func (config *Config) SetDataString(key, data string) error {
	value, in := config.table[key]
	if !in {
		value = newConfigNode(ConfigString, data, key)
	} else {
		if value.nodeType != ConfigString {
			return errors.New("key is not a string")
		}
		value.value = data
	}
	config.table[key] = value
	return nil
}

func (nodeSlice configNodeSlice) Len() int {
	return len(nodeSlice)
}

func (nodeSlice configNodeSlice) Less(i, j int) bool {
	if nodeSlice[i].nodeType == nodeSlice[j].nodeType {
		switch nodeSlice[i].nodeType {
		case ConfigBool:
			return nodeSlice[i].value.(bool) == nodeSlice[j].value.(bool)
		case ConfigNum:
			return nodeSlice[i].value.(float64) < nodeSlice[j].value.(float64)
		case ConfigVector:
			return len(nodeSlice[i].value.([]float64)) < len(nodeSlice[j].value.([]float64))
		case ConfigString:
			return nodeSlice[i].value.(string) < nodeSlice[j].value.(string)
		}
	}
	return nodeSlice[i].nodeType < nodeSlice[j].nodeType
}

func (nodeSlice configNodeSlice) Swap(i, j int) {
	nodeSlice[i], nodeSlice[j] = nodeSlice[j], nodeSlice[i]
}


func (config *Config) writeNodes(file *os.File) {
	values := configNodeSlice{}
	for _, val := range config.table {
		values = append(values, val)
	}
	sort.Sort(values)
	for i := 0; i < len(values); i++ {
		val := values[i]
		str := make([]byte, 0)
		str = append(str, val.key...)
		str = append(str, ' ')
		switch val.nodeType {
		case ConfigBool:
			boolVal := strings.ToUpper(fmt.Sprint(val.value.(bool)))
			str = append(str, boolVal...)
		case ConfigNum:
			str = strconv.AppendFloat(str, val.value.(float64), 'f', -1, 64)
		case ConfigVector:
			vals := val.value.([]float64)
			for i := 0; i < len(vals); i++ {
				str = strconv.AppendFloat(str, vals[i], 'f', -1, 64)
				if i < len(vals)-1 {
					str = append(str, ',')
				}
			}
		case ConfigString:
			str = append(str, []byte(val.value.(string))...)
		}
		str = append(str, '\n')
		file.Write(str)
	}
}

func (config *Config) WriteToFile(filename string) error {
	if filename == "" && config.filename != "" {
		file, err := os.OpenFile(config.filename, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			return err
		}
		defer file.Close()
		config.writeNodes(file)
	} else if filename != "" {
		file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			return err
		}
		defer file.Close()
		config.writeNodes(file)
	} else {
		return errors.New("filename or the filename stored in the config must be a valid path")
	}
	return nil
}
