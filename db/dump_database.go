package db

import (
	"bytes"
	"encoding/gob"
	"os"
)

func (d *Database) createHeader() []byte {
	header := createHeader(d.header.compressionMethod, d.header.pages, d.header.maxNodesPerPage)
	return header
}

func (p *page) createHeader() []byte {
	header := createHeader(p.header.nodesLength, p.header.uncompressedSize, p.header.compressedSize)
	return header
}

func (n *node) createHeader() []byte {
	header := createHeader(n.header.size, n.header.key)
	return header
}

func (n *node) getNode() ([]byte, error) {
	output := make([]byte, 0)
	header := n.createHeader()
	output = append(output, header...)
	buffer := bytes.Buffer{}
	enc := gob.NewEncoder(&buffer)
	err := enc.Encode(&n.value)
	if err != nil {
		return nil, err
	}
	output = append(output, buffer.Bytes()...)
	return output, err
}

func compressPage(data []byte, compressionMethod string) ([]byte, error) {
	algorithm, ok := algorithms[compressionMethod]
	if !ok {
		return nil, invalidCompression
	}
	return algorithm.compress(data)
}

func (p *page) dumpPage(file *os.File, compressionMethod string) error {
	pageData := make([]byte, 0)
	for _, n := range p.nodes {
		data, err := n.getNode()
		if err != nil {
			return err
		}
		pageData = append(pageData, data...)
	}
	compressedData, err := compressPage(pageData, compressionMethod)
	if err != nil {
		return err
	}
	p.header.compressedSize = uint32(len(compressedData))
	header := p.createHeader()
	pageData = append(header, compressedData...)
	_, err = file.Write(pageData)
	if err != nil {
		return err
	}
	return nil
}

// Dump the current database to the file it was created with.
func (d *Database) Dump() error {
	file, err := os.OpenFile(d.name, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		return err
	}
	defer file.Close()
	file.Truncate(0)
	file.Seek(0, 0)
	header := d.createHeader()
	_, err = file.Write(header)
	if err != nil {
		return err
	}
	for i := 0; i < int(d.header.pages); i++ {
		err = d.pages[i].dumpPage(file, d.header.compressionMethod)
		if err != nil {
			return err
		}
	}
	d.header.hash, err = hashFile(d.name)
	return err
}
