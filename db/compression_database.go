package db

import (
	"bytes"
	"compress/flate"
	"compress/gzip"
	"compress/lzw"
	"compress/zlib"
	"io"
)

type compressionFunc func([]byte) ([]byte, error)

type algorithm struct {
	compress   compressionFunc
	decompress compressionFunc
}

var algorithms map[string]algorithm

func copyCompress(data []byte) ([]byte, error) {
	return data, nil
}

func copyDecompress(data []byte) ([]byte, error) {
	return data, nil
}

func flateCompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	zw, err := flate.NewWriter(buffer, flate.DefaultCompression)
	if err != nil {
		return nil, err
	}
	_, err = zw.Write(data)
	if err != nil {
		return nil, err
	}
	err = zw.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func flateDecompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	compressedBuffer := bytes.NewBuffer(data)
	zr := flate.NewReader(compressedBuffer)
	tmp := make([]byte, 1024)
	for {
		bytesRead, err := zr.Read(tmp)
		if err != nil && err != io.EOF {
			return nil, err
		}
		buffer.Write(tmp[:bytesRead])
		if err == io.EOF {
			break
		}
	}
	err := zr.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func gzipCompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	zw, err := gzip.NewWriterLevel(buffer, gzip.DefaultCompression)
	if err != nil {
		return nil, err
	}
	_, err = zw.Write(data)
	if err != nil {
		return nil, err
	}
	err = zw.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func gzipDecompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	compressedBuffer := bytes.NewBuffer(data)
	zr, err := gzip.NewReader(compressedBuffer)
	if err != nil {
		return nil, err
	}
	tmp := make([]byte, 1024)
	for {
		bytesRead, err := zr.Read(tmp)
		if err != nil && err != io.EOF {
			return nil, err
		}
		buffer.Write(tmp[:bytesRead])
		if err == io.EOF {
			break
		}
	}
	err = zr.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func lzwCompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	zw := lzw.NewWriter(buffer, lzw.LSB, 8)
	_, err := zw.Write(data)
	if err != nil {
		return nil, err
	}
	err = zw.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func lzwDecompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	compressedBuffer := bytes.NewBuffer(data)
	zr := lzw.NewReader(compressedBuffer, lzw.LSB, 8)
	tmp := make([]byte, 1024)
	for {
		bytesRead, err := zr.Read(tmp)
		if err != nil && err != io.EOF {
			return nil, err
		}
		buffer.Write(tmp[:bytesRead])
		if err == io.EOF {
			break
		}
	}
	err := zr.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func zlibCompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	zw := zlib.NewWriter(buffer)
	_, err := zw.Write(data)
	if err != nil {
		return nil, err
	}
	err = zw.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func zlibDecompress(data []byte) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	compressedBuffer := bytes.NewBuffer(data)
	zr, err := zlib.NewReader(compressedBuffer)
	if err != nil {
		return nil, err
	}
	tmp := make([]byte, 1024)
	for {
		bytesRead, err := zr.Read(tmp)
		if err != nil && err != io.EOF {
			return nil, err
		}
		buffer.Write(tmp[:bytesRead])
		if err == io.EOF {
			break
		}
	}
	err = zr.Close()
	if err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

// Adds a compression algorithm to the list of algorithms. The functions must return a byte array with the compressed/decompressed data.
// See the above functions for examples
func (d *Database) AddCompressionAlgorithm(name string, compress, decompress compressionFunc) {
	algorithms[name] = algorithm{compress, decompress}
}

// Returns all the algorithms that are currently registered.
// This function iterates through the internal map so it is not free to call.
func (d *Database) GetCompressionAlgorithms() []string {
	names := make([]string, len(algorithms))
	i := 0
	for k := range algorithms {
		names[i] = k
		i++
	}
	return names
}

func (d *Database) initCompression() {
	algorithms = make(map[string]algorithm)
	d.AddCompressionAlgorithm("ZLIB", zlibCompress, zlibDecompress)
	d.AddCompressionAlgorithm("LZW", lzwCompress, lzwDecompress)
	d.AddCompressionAlgorithm("GZIP", gzipCompress, gzipDecompress)
	d.AddCompressionAlgorithm("FLATE", flateCompress, flateDecompress)
	d.AddCompressionAlgorithm("COPY", copyCompress, copyDecompress)
}
