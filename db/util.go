package db

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
	"reflect"
	"strconv"
)

func createHeader(items ...interface{}) []byte {
	databaseHeader := make([]byte, 0)
	databaseHeader = append(databaseHeader, 1)
	for i := 0; i < len(items); i++ {
		switch v := items[i].(type) {
		case int, int8, int16, int32, int64:
			databaseHeader = strconv.AppendInt(databaseHeader, reflect.ValueOf(v).Int(), 10)
		case uint, uint8, uint16, uint32, uint64:
			databaseHeader = strconv.AppendUint(databaseHeader, reflect.ValueOf(v).Uint(), 10)
		case float32:
			databaseHeader = strconv.AppendFloat(databaseHeader, reflect.ValueOf(v).Float(), 'f', 5, 32)
		case float64:
			databaseHeader = strconv.AppendFloat(databaseHeader, reflect.ValueOf(v).Float(), 'f', 5, 64)
		case string:
			databaseHeader = append(databaseHeader, reflect.ValueOf(v).String()...)
		case []byte:
			databaseHeader = append(databaseHeader, reflect.ValueOf(v).Bytes()...)
		}
		if i != len(items)-1 {
			databaseHeader = append(databaseHeader, 2)
		}
	}
	databaseHeader = append(databaseHeader, 1)
	return databaseHeader
}

func hashFile(name string) (string, error) {
	file, err := os.OpenFile(name, os.O_RDONLY, 0755)
	if err != nil {
		return "", err
	}
	defer file.Close()
	hasher := md5.New()
	length, err := file.Seek(0, os.SEEK_END)
	if err != nil {
		return "", err
	}
	_, _ = file.Seek(0, os.SEEK_CUR)
	data := make([]byte, readSize)
	for i := int64(0); i < length/readSize; i += readSize {
		bytesRead, err := file.Read(data)
		if err != nil && err != io.EOF {
			return "", err
		}
		_, err = hasher.Write(data[:bytesRead])
		if err != nil {
			return "", err
		}
	}
	hash := hasher.Sum(nil)[:16]
	return hex.EncodeToString(hash), nil
}
