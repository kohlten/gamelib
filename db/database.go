package db

import (
	"bytes"
	"encoding/gob"
	"errors"
	"io"
	"os"
	"reflect"
	"strings"
)

var invalidName = errors.New("invalid name of archive")
var invalidCompression = errors.New("invalid compression algorithm")
var invalidHeader = errors.New("header of archive is corrupt")
var invalidPageHeader = errors.New("page header of archive is corrupt")
var invalidNodeHeader = errors.New("node header of archive is corrupt")
var invalidData = errors.New("page has been corrupted")
var notFound = errors.New("value was not found")

const (
	DefaultMaxNodesPerPage = 100

	readSize = 1024

	start = 1
	end   = 1
	sep   = 2
)

var initialized = false

// @Todo if a node is particularly large, have it be in it's own page
// @Todo Separate update into different functions
// @Todo serialize the header data in a different way
// @Todo make the parsing of each header modular

type databaseHeader struct {
	compressionMethod string
	pages             uint32
	maxNodesPerPage   uint32
	hash              string
}

type nodeHeader struct {
	t    string
	size uint32
	key  string
}

type node struct {
	header nodeHeader
	value  interface{}
}

type pageHeader struct {
	nodesLength      uint32
	uncompressedSize uint32
	compressedSize   uint32
	hash             string
}

type page struct {
	header pageHeader
	nodes  map[string]*node
}

type Database struct {
	name   string
	pages  []*page
	header databaseHeader
}

func validCompressionMethod(method string) bool {
	method = strings.Trim(method, " \t\r\n\f\v")
	method = strings.ToUpper(method)
	_, ok := algorithms[method]
	return ok
}

// Create a new empty database. It will not write to a file until you call Dump.
func NewDatabase(name string, compressionMethod string, maxNodesPerPage int) (*Database, error) {
	if name == "" {
		return nil, invalidName
	}
	d := new(Database)
	d.name = name
	d.pages = make([]*page, 0)
	d.header.compressionMethod = strings.ToUpper(compressionMethod)
	d.header.maxNodesPerPage = uint32(maxNodesPerPage)
	if !initialized {
		d.initCompression()
		initialized = true
	}
	if !validCompressionMethod(compressionMethod) {
		return nil, invalidCompression
	}
	return d, nil
}

// Loads a database from a file.
func LoadDatabase(name string) (*Database, error) {
	d, err := NewDatabase(name, "COPY", DefaultMaxNodesPerPage)
	if err != nil {
		return nil, err
	}
	err = d.Update()
	if err != nil {
		return nil, err
	}
	return d, nil
}

func readBytesFromFile(buffer *[]byte, file *os.File) error {
	tmp := make([]byte, readSize)
	num, err := file.Read(tmp)
	if err != nil {
		if err != io.EOF {
			return err
		}
	}
	if err != io.EOF {
		*buffer = append(*buffer, tmp[:num]...)
	}
	return err
}

func (d *Database) checkIfUpdateNeeded() (bool, error) {
	_, err := os.Stat(d.name)
	if err != nil {
		return false, nil
	}
	hash, err := hashFile(d.name)
	if err != nil {
		return false, err
	}
	if hash == d.header.hash {
		return false, nil
	}
	d.header.hash = hash
	return true, nil
}

// Update the database information from the file
func (d *Database) Update() error {
	updateNeeded, err := d.checkIfUpdateNeeded()
	if err != nil {
		return err
	}
	if !updateNeeded {
		return nil
	}
	file, err := os.OpenFile(d.name, os.O_RDONLY, 0755)
	defer file.Close()
	buffer := make([]byte, 0)
	finishedReading := false
	err = readBytesFromFile(&buffer, file)
	if err != nil {
		if err == io.EOF {
			finishedReading = true
		} else {
			return err
		}
	}
	offset, err := d.parseHeader(buffer)
	if err != nil {
		return err
	}
	buffer = buffer[offset:]
	d.pages = make([]*page, 0)
	for {
		if len(buffer) == 0 {
			break
		}
		pageInfo := new(page)
		if !finishedReading {
			err = readBytesFromFile(&buffer, file)
			if err == io.EOF {
				finishedReading = true
			} else if err != nil {
				return err
			}
		}
		offset, err := parsePageHeader(&pageInfo.header, buffer)
		if err != nil {
			return err
		}
		buffer = buffer[offset:]
		for len(buffer) < int(pageInfo.header.compressedSize) {
			if !finishedReading {
				err = readBytesFromFile(&buffer, file)
				if err == io.EOF {
					finishedReading = true
				} else if err != nil {
					return err
				}
			} else {
				return invalidData
			}
		}
		_, err = d.parsePage(pageInfo, buffer[:pageInfo.header.compressedSize])
		if err != nil {
			return err
		}
		d.pages = append(d.pages, pageInfo)
		buffer = buffer[pageInfo.header.compressedSize:]
	}
	return nil
}

func (d *Database) newPage() {
	newPage := new(page)
	newPage.nodes = make(map[string]*node)
	d.pages = append(d.pages, newPage)
	d.header.pages++
}

func (p *page) updatePageHeader() {
	p.header.uncompressedSize = 0
	p.header.nodesLength = 0
	for _, n := range p.nodes {
		header := n.createHeader()
		p.header.uncompressedSize += n.header.size + uint32(len(header))
		p.header.nodesLength++
	}
	p.header.compressedSize = p.header.uncompressedSize
}

func (d *Database) addPageNode(pageInfo *page, key string, value interface{}) error {
	newNode := new(node)
	newNode.header.key = key
	newNode.header.t = reflect.ValueOf(value).Type().String()
	buffer := bytes.Buffer{}
	enc := gob.NewEncoder(&buffer)
	err := enc.Encode(&value)
	if err != nil {
		return err
	}
	newNode.header.size = uint32(buffer.Len())
	newNode.value = value
	pageInfo.nodes[key] = newNode
	pageInfo.updatePageHeader()
	return nil
}

// Add a new values to the database. It will not write to the file until you call Dump.
// To add a new struct you first need to call gob.Register on that struct.
func (d *Database) SetValue(key string, value interface{}) error {
	page := 0
	foundPage := false
	for ; page < int(d.header.pages); page++ {
		if d.pages[page].header.nodesLength < d.header.maxNodesPerPage {
			foundPage = true
			break
		}
	}
	if !foundPage {
		d.newPage()
		page = len(d.pages) - 1
	}
	err := d.addPageNode(d.pages[page], key, value)
	if err != nil {
		return err
	}
	return nil
}

// Retrieve a value from the database. If the value could not be found, it will return not found in the error.
func (d *Database) GetValue(key string) (value interface{}, valType string, err error) {
	for i := 0; i < int(d.header.pages); i++ {
		val, ok := d.pages[i].nodes[key]
		if ok {
			return val.value, val.header.t, nil
		}
	}
	return nil, "", notFound
}
