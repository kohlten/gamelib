package db

import (
	"bytes"
	"encoding/gob"
	"reflect"
	"strconv"
)

func (d *Database) parseHeader(data []byte) (int, error) {
	i := 0
	if data[i] != start {
		return 0, invalidHeader
	}
	i++
	compressionMethod := make([]byte, 0)
	for ; i < len(data) && data[i] != sep; i++ {
		compressionMethod = append(compressionMethod, data[i])
	}
	if i == len(data) || data[i] != sep {
		return 0, invalidHeader
	}
	i++
	stringNum := make([]byte, 0)
	for ; i < len(data) && data[i] != sep; i++ {
		stringNum = append(stringNum, data[i])
	}
	if i == len(data) || data[i] != sep {
		return 0, invalidHeader
	}
	i++
	pages, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidHeader
	}
	stringNum = make([]byte, 0)
	for ; i < len(data) && data[i] != end; i++ {
		stringNum = append(stringNum, data[i])
	}
	if data[i] != end {
		return 0, invalidHeader
	}
	i++
	nodesPerPage, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidHeader
	}
	d.header.compressionMethod = string(compressionMethod)
	d.header.pages = uint32(pages)
	d.header.maxNodesPerPage = uint32(nodesPerPage)
	return i, nil
}

func parseNodeHeader(header *nodeHeader, data []byte) (int, error) {
	i := 0
	if data[i] != start {
		return 0, invalidNodeHeader
	}
	i++
	stringNum := make([]byte, 0)
	for ; i < len(data) && data[i] != sep; i++ {
		stringNum = append(stringNum, data[i])
	}
	if i == len(data) || data[i] != sep {
		return 0, invalidNodeHeader
	}
	i++
	size, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidNodeHeader
	}
	key := make([]byte, 0)
	for ; i < len(data) && data[i] != end; i++ {
		key = append(key, data[i])
	}
	if data[i] != end {
		return 0, invalidNodeHeader
	}
	i++
	header.size = uint32(size)
	header.key = string(key)
	return i, nil
}

func parsePageHeader(header *pageHeader, data []byte) (int, error) {
	i := 0
	if data[i] != start {
		return 0, invalidPageHeader
	}
	i++
	stringNum := make([]byte, 0)
	for ; i < len(data) && data[i] != sep; i++ {
		stringNum = append(stringNum, data[i])
	}
	if i == len(data) || data[i] != sep {
		return 0, invalidPageHeader
	}
	i++
	nodes, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidPageHeader
	}
	stringNum = make([]byte, 0)
	for ; i < len(data) && data[i] != sep; i++ {
		stringNum = append(stringNum, data[i])
	}
	if i == len(data) || data[i] != sep {
		return 0, invalidPageHeader
	}
	i++
	uncompressedSize, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidPageHeader
	}
	stringNum = make([]byte, 0)
	for ; i < len(data) && data[i] != end; i++ {
		stringNum = append(stringNum, data[i])
	}
	if data[i] != end {
		return 0, invalidPageHeader
	}
	i++
	compressedSize, err := strconv.ParseUint(string(stringNum), 10, 32)
	if err != nil {
		return 0, invalidPageHeader
	}
	header.nodesLength = uint32(nodes)
	header.uncompressedSize = uint32(uncompressedSize)
	header.compressedSize = uint32(compressedSize)
	return i, nil
}

func parseNode(data []byte) (*node, uint, error) {
	dataNode := new(node)
	total, err := parseNodeHeader(&dataNode.header, data)
	if err != nil {
		return nil, 0, err
	}
	valueData := data[total : total+int(dataNode.header.size)]
	buffer := bytes.NewBuffer(valueData)
	dec := gob.NewDecoder(buffer)
	err = dec.Decode(&dataNode.value)
	if err != nil {
		return nil, 0, err
	}
	dataNode.header.t = reflect.ValueOf(dataNode.value).Type().String()
	return dataNode, uint(uint32(total) + dataNode.header.size), nil
}

func (d *Database) decompressPage(data []byte) ([]byte, error) {
	algorithm, ok := algorithms[d.header.compressionMethod]
	if !ok {
		return nil, invalidCompression
	}
	return algorithm.decompress(data)
}

func (d *Database) parsePage(pageInfo *page, data []byte) (uint, error) {
	total := uint(0)
	pageInfo.nodes = make(map[string]*node)
	if uint32(len(data)) < pageInfo.header.compressedSize {
		return 0, invalidData
	}
	uncompressedPageData, err := d.decompressPage(data)
	if err != nil {
		return 0, err
	}
	if uint32(len(uncompressedPageData)) != pageInfo.header.uncompressedSize {
		return 0, invalidData
	}
	totalOffset := uint(0)
	for i := uint32(0); i < pageInfo.header.nodesLength; i++ {
		node, offset, err := parseNode(uncompressedPageData[totalOffset:])
		if err != nil {
			return 0, err
		}
		pageInfo.nodes[node.header.key] = node
		totalOffset += offset
	}
	return total + uint(pageInfo.header.compressedSize), nil
}
