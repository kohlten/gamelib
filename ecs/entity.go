package ecs

type EntityState int

const (
	EntityAsleep = EntityState(1 << 0)
	EntityActive = EntityState(1 << 1)
)

type Entity struct {
	Id uint
	State EntityState
	components []*Component
}

type EntityHandle struct {
	entity *Entity
	world *World
}

func NewEntityHandle(entity *Entity, world *World) *EntityHandle {
	eh := new(EntityHandle)
	eh.entity = entity
	eh.world = world
	return eh
}

func (eh *EntityHandle) AddComponent(data interface{}, name string) error {
	component := NewComponent(data, name, eh.entity)
	for i := 0; i < len(eh.entity.components); i++ {
		if eh.entity.components[i].Name == name {
			return ComponentExists
		}
	}
	eh.entity.components = append(eh.entity.components, component)
	return nil
}

func (eh *EntityHandle) RemoveComponent(name string) error {
	for i := 0; i < len(eh.entity.components); i++ {
		if eh.entity.components[i].Name == name {
			eh.entity.components = append(eh.entity.components[:i], eh.entity.components[i + 1:]...)
			return nil
		}
	}
	return ComponentNotFound
}

func (eh *EntityHandle) GetComponent(name string) (*Component, error) {
	for i := 0; i < len(eh.entity.components); i++ {
		if eh.entity.components[i].Name == name {
			return eh.entity.components[i], nil
		}
	}
	return nil, ComponentNotFound
}

func (eh *EntityHandle) SetState(state EntityState) {
	eh.entity.State = state
}

func (eh *EntityHandle) GetEntity() *Entity {
	return eh.entity
}
