package ecs

type Component struct {
	owner *Entity
	Data interface{}
	Name string
}


func NewComponent(data interface{}, componentName string, owner *Entity) *Component {
	component := new(Component)
	component.owner = owner
	component.Name = componentName
	component.Data = data
	return component
}

func (ch *Component) GetOwner() *Entity {
	return ch.owner
}

