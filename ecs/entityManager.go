package ecs

type EntityManager struct {
	entities []*Entity
	lastEntity uint
}

func newEntityManager() *EntityManager {
	return new(EntityManager)
}

func (em *EntityManager) createEntity() *Entity {
	newEntity := new(Entity)
	newEntity.Id = em.lastEntity
	newEntity.State = EntityActive
	em.entities = append(em.entities, newEntity)
	em.lastEntity++
	return newEntity
}

func (em *EntityManager) addEntity(entity *Entity) error {
	if em.entityExists(entity) {
		return EntityExists
	}
	em.entities = append(em.entities, entity)
	return nil
}

func (em *EntityManager) removeEntity(entity *Entity) error {
	for i := 0; i < len(em.entities); i++ {
		if em.entities[i].Id == entity.Id {
			em.entities = append(em.entities[:i], em.entities[i + 1:]...)
			return nil
		}
	}
	return nil
}

func (em *EntityManager) entityExists(entity *Entity) bool {
	for i := 0; i < len(em.entities); i++ {
		if em.entities[i].Id == entity.Id {
			return true
		}
	}
	return false
}
