package ecs

type SystemType interface {
	Init(system *System, data ...interface{})
	Update(system *System, dt int, data ...interface{})
	Render(system *System, data ...interface{})
}

type System struct {
	registeredEntities []*Entity
	asleepEntities     []*Entity
	parent             *World
	system             SystemType
}

func NewSystem(system SystemType) *System {
	sys := new(System)
	sys.system = system
	return sys
}

func (system *System) RegisterWorld(world *World) {
	system.parent = world
}

func (system *System) GetEntities() []*Entity {
	return system.registeredEntities
}

func (system *System) GetParent() *World {
	return system.parent
}

func (system *System) RegisterEntity(entity *Entity) error {
	for i := 0; i < len(system.registeredEntities); i++ {
		if system.registeredEntities[i].Id == entity.Id {
			return EntityInSystem
		}
	}
	for i := 0; i < len(system.asleepEntities); i++ {
		if system.asleepEntities[i].Id == entity.Id {
			return EntityInSystem
		}
	}
	if entity.State == EntityActive {
		system.registeredEntities = append(system.registeredEntities, entity)
	} else {
		system.asleepEntities = append(system.asleepEntities, entity)
	}
	return nil
}

func (system *System) UnregisterEntity(entity *Entity) error {
	for i := 0; i < len(system.registeredEntities); i++ {
		if system.registeredEntities[i].Id == entity.Id {
			system.registeredEntities = append(system.registeredEntities[:i], system.registeredEntities[i + 1:]...)
			return nil
		}
	}
	for i := 0; i < len(system.asleepEntities); i++ {
		if system.asleepEntities[i].Id == entity.Id {
			system.asleepEntities = append(system.asleepEntities[:i], system.asleepEntities[i + 1:]...)
			return nil
		}
	}
	return EntityNotFound
}

func (system *System) updateEntities() {
	for i := 0; i < len(system.registeredEntities); i++ {
		if system.registeredEntities[i].State == EntityAsleep {
			entity := system.registeredEntities[i]
			system.registeredEntities = append(system.registeredEntities[:i], system.registeredEntities[i + 1:]...)
			system.asleepEntities = append(system.asleepEntities, entity)
		}
	}
	for i := 0; i < len(system.asleepEntities); i++ {
		if system.asleepEntities[i].State == EntityActive {
			entity := system.asleepEntities[i]
			system.asleepEntities = append(system.asleepEntities[:i], system.asleepEntities[i + 1:]...)
			system.registeredEntities = append(system.registeredEntities, entity)
		}
	}
}

func (system *System) entityInSystem(entity *Entity) bool {
	for i := 0; i < len(system.registeredEntities); i++ {
		if system.registeredEntities[i].Id == entity.Id {
			return true
		}
	}
	for i := 0; i < len(system.asleepEntities); i++ {
		if system.asleepEntities[i].Id == entity.Id {
			return true
		}
	}
	return false
}

func (system *System) Init(data ...interface{}) {
	system.updateEntities()
	system.system.Init(system, data)
}

func (system *System) Update(dt int, data ...interface{}) {
	system.updateEntities()
	system.system.Update(system, dt, data)
}

func (system *System) Render(data ...interface{}) {
	system.updateEntities()
	system.system.Render(system, data)
}



