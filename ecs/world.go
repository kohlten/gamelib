package ecs

import (
	"errors"
)

// @TODO Make the system updating multithreaded and see if that will work
// @TODO See if rendering needs to be removed

var (
	SystemExists = errors.New("system already exists under that name")
	SystemNotFound = errors.New("system was not found")
	EntityExists = errors.New("entity already exists under that id")
	EntityNotFound = errors.New("entity was not found")
	EntityInSystem = errors.New("entity is already in the system")
	ComponentExists = errors.New("component already exists under that name")
	ComponentNotFound = errors.New("component was not found")
)

type World struct {
	entityManager *EntityManager
	systems map[string]*System
}

func NewWorld() *World {
	world := new(World)
	world.entityManager = newEntityManager()
	world.systems = make(map[string]*System)
	return world
}

// Creates a new entity with no components
func (world *World) CreateEntity() *EntityHandle {
	return NewEntityHandle(world.entityManager.createEntity(), world)
}

func (world *World) RemoveEntity(entity *EntityHandle) error {
	for name := range world.systems {
		if world.EntityInSystem(entity.entity, name) {
			err := world.RemoveEntityFromSystem(entity.entity, name)
			if err != nil {
				return err
			}
		}
	}
	return world.entityManager.removeEntity(entity.entity)
}

// Adds a previously made entity
func (world *World) AddEntity(handle *EntityHandle) error {
	return world.entityManager.addEntity(handle.entity)

}

func (world *World) AddSystem(system *System, name string) error {
	system.RegisterWorld(world)
	if _, ok := world.systems[name]; ok {
		return SystemExists
	}
	world.systems[name] = system
	return nil
}

func (world *World) RemoveSystem(name string) error {
	if _, ok := world.systems[name]; ok {
		delete(world.systems, name)
		return nil
	}
	return SystemNotFound
}

func (world *World) AddEntityToSystem(entity *Entity, name string) error {
	system, ok := world.systems[name]
	if !ok {
		return SystemNotFound
	}
	return system.RegisterEntity(entity)
}

func (world *World) RemoveEntityFromSystem(entity *Entity, name string) error {
	return world.systems[name].UnregisterEntity(entity)
}

func (world *World) EntityInSystem(entity *Entity, name string) bool {
	return world.systems[name].entityInSystem(entity)
}

func (world *World) Init(data ...interface{}) {
	for _, value := range world.systems {
		value.system.Init(value, data)
	}
}

func (world *World) Update(dt int, data ...interface{}) {
	for _, system := range world.systems {
		system.Update(dt, data)
	}
}

func (world *World) Render(data ...interface{}) {
	for _, system := range world.systems {
		system.Render(data)
	}
}

