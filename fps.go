package gamelib

import (
	"time"
)

type FpsCounter struct {
	frames uint32
	fps uint32
	timer *Timer
}

func NewFpsCounter() *FpsCounter {
	counter := new(FpsCounter)
	counter.timer = NewTimer(1000, TimeMilliseconds)
	counter.timer.Start()
	return counter
}

func (counter *FpsCounter) Update() {
	counter.frames++
	counter.timer.Update()
	if counter.timer.IsCompleted() {
		counter.fps = counter.frames
		counter.frames = 0
		counter.timer.Start()
	}
}

func (counter *FpsCounter) GetFps() uint32 {
	return counter.fps
}

// This function uses nanoseconds, you can get the startTime by calling GetTimeNS()
func LimitFPS(frameStartTime uint64, targetFPS float64) {
	timePerFrame := (1000.0 / targetFPS) * 1000000.0
	currentTime := GetTimeNS()
	if float64(currentTime) - float64(frameStartTime) < timePerFrame {
		sleepTime := timePerFrame - (float64(currentTime) - float64(frameStartTime))
		time.Sleep(time.Duration(sleepTime) * time.Nanosecond)
	}
}

