package gamelib

/*import (
    "errors"
    "io/ioutil"

    "github.com/go-gl/gl/all-core/gl"
)

type Shader struct {
	Id uint32
}

func getOpenGLShaderError(shader uint32) string {
	length := int32(0)
	errorData := string(make([]byte, 400))
	gl.GetShaderInfoLog(shader, 400, &length, gl.Str(errorData))
	return errorData
}

func getOpenGLProgramError(program uint32) string {
	length := int32(0)
	errorData := string(make([]byte, 400))
	gl.GetProgramInfoLog(program, 400, &length, gl.Str(errorData))
	return errorData
}

func createProgram(vertexSource, fragSource string) (uint32, error) {
	vs := gl.CreateShader(gl.VERTEX_SHADER)
	glSource, free := gl.Strs(vertexSource)
	gl.ShaderSource(vs, 1, glSource, nil)
	gl.CompileShader(vs)
	status := int32(0)
	free()
	gl.GetShaderiv(vs, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		errorData := getOpenGLShaderError(vs)
		return 0, errors.New(errorData)
	}
	fs := gl.CreateShader(gl.FRAGMENT_SHADER)
	glSource, free = gl.Strs(fragSource)
	gl.ShaderSource(fs, 1, glSource, nil)
	gl.CompileShader(fs)
	status = int32(0)
	free()
	gl.GetShaderiv(fs, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		errorData := getOpenGLShaderError(fs)
		return 0, errors.New(errorData)
	}
	program := gl.CreateProgram()
	gl.AttachShader(program, vs)
	gl.AttachShader(program, fs)
	gl.LinkProgram(program)
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		errorData := getOpenGLProgramError(program)
		return 0, errors.New(errorData)
	}
	return program, nil
}

func NewShader(vertex, frag string) (*Shader, error) {
	vertexSourceBytes, err := ioutil.ReadFile(vertex)
	if err != nil {
		return nil, err
	}
	fragSourceBytes, err := ioutil.ReadFile(frag)
	if err != nil {
		return nil, err
	}
	vertexSource := string(vertexSourceBytes) + "\x00"
	fragSource := string(fragSourceBytes) + "\x00"
	shader := new(Shader)
	shader.Id, err = createProgram(vertexSource, fragSource)
	if err != nil {
		return nil, err
	}
	return shader, nil
}

func NewShaderFromString(vertexSource, fragSource string) (*Shader, error) {
	vertexSource += "\x00"
	fragSource += "\x00"
	shader := new(Shader)
	id, err := createProgram(vertexSource, fragSource)
	if err != nil {
		return nil, err
	}
	shader.Id = id
	return shader, nil
}

func (shader *Shader) Use() {
	gl.UseProgram(shader.Id)
}

func (shader *Shader) SetBool(name string, value bool) {
	v := 0
	if value {
		v = 1
	}
	gl.Uniform1i(gl.GetUniformLocation(shader.Id, gl.Str(name)), int32(v))
}

func (shader *Shader) SetInt(name string, value int) {
	gl.Uniform1i(gl.GetUniformLocation(shader.Id, gl.Str(name)), int32(value))
}

func (shader *Shader) SetFloat(name string, value float32) {
	gl.Uniform1f(gl.GetUniformLocation(shader.Id, gl.Str(name)), value)
}*/
