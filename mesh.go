package gamelib
/*
import (
	"github.com/go-gl/gl/all-core/gl"
	"github.com/ungerik/go3d/float64/vector.Vec2"
	"github.com/ungerik/go3d/float64/vec3"
	"gitlab.com/kohlten/gamelib/obj"
	"unsafe"
)

type Mesh struct {
	Vertices []Vertex
	Indices  []uint32
	Textures []*Texture
	vao      uint32
	vbo      uint32
	ebo      uint32
}

func (mesh *Mesh) setup() {
	gl.GenVertexArrays(1, &mesh.vao)
	gl.GenBuffers(1, &mesh.vbo)
	gl.GenBuffers(1, &mesh.ebo)

	vertexSize := unsafe.Sizeof(mesh.Vertices[0])
	vec3Size := unsafe.Sizeof(mesh.Vertices[0].Position)
	indexSize := unsafe.Sizeof(mesh.Indices[0])

	gl.BindVertexArray(mesh.vao)
	gl.BindBuffer(gl.ARRAY_BUFFER, mesh.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(mesh.Vertices)*int(vertexSize), gl.Ptr(mesh.Vertices[0]), gl.STATIC_DRAW)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(mesh.Indices)*int(indexSize), gl.Ptr(mesh.Indices[0]), gl.STATIC_DRAW)

	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, int32(vertexSize), gl.PtrOffset(0*int(vec3Size)))

	gl.EnableVertexAttribArray(1)
	gl.VertexAttribPointer(1, 3, gl.FLOAT, false, int32(vertexSize), gl.PtrOffset(1*int(vec3Size)))

	gl.EnableVertexAttribArray(2)
	gl.VertexAttribPointer(2, 2, gl.FLOAT, false, int32(vertexSize), gl.PtrOffset(2*int(vec3Size)))

	gl.BindVertexArray(0)
}

func NewMesh(vertices []Vertex, indices []uint32, textures []*Texture) *Mesh {
	mesh := new(Mesh)
	mesh.Vertices = vertices
	mesh.Indices = indices
	mesh.Textures = textures
	mesh.setup()
	return mesh
}

func mapTextures(obj *obj.Obj) ([]*Texture, error) {
	textures := make([]*Texture, 0)
	if obj.Material.MapAmbient.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapAmbient.Filename, "ambient")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.MapDiffuse.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapDiffuse.Filename, "diffuse")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.MapSpecular.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapDiffuse.Filename, "specular")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.MapSpecularExponent.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapSpecularExponent.Filename, "specular_exponent")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.MapTransparency.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapTransparency.Filename, "transparency")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.MapBump.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.MapBump.Filename, "bump")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.Reflection.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.Reflection.Filename, "reflection")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.Decal.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.Decal.Filename, "decal")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	if obj.Material.Displacement.Filename != "" {
		texture, err := NewTextureFromFile(obj.Material.Displacement.Filename, "displacement")
		if err != nil {
			return nil, err
		}
		textures = append(textures, texture)
	}
	return textures, nil
}

func NewMeshFromObj(obj *obj.Obj) (*Mesh, error) {
	vertices := make([]Vertex, len(obj.GeometricVertices))
	for i := 0; i < len(obj.GeometricVertices); i++ {
		pos := obj.GeometricVertices[i]
		normal := obj.VertexNormals[i]
		texCoord := obj.TextureVertices[i]
		vertex := NewVertex(vec3.T{pos[0], pos[1], pos[2]}, vec3.T{normal[0], normal[1], normal[2]}, vector.Vec2{texCoord[0], texCoord[1]})
		vertices[i] = vertex
	}
	indices := make([]uint32, len(obj.Polygons)*3)
	for i := 0; i < len(obj.Polygons); i++ {
		for j := 0; j < len(obj.Polygons[i]); j++ {
			indices = append(indices, uint32(obj.Polygons[i][j]))
		}
	}
	textures, err := mapTextures(obj)
	if err != nil {
		return nil, err
	}
	return NewMesh(vertices, indices, textures), nil
}

func (mesh *Mesh) Draw(shader *Shader) {
	for i := 0; i < len(mesh.Textures); i++ {
		gl.ActiveTexture(uint32(gl.TEXTURE0 + i))
		shader.SetInt("material." + mesh.Textures[i].Type, i)
		gl.BindTexture(gl.TEXTURE_2D, mesh.Textures[i].Id)
	}
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindVertexArray(mesh.vao)
	gl.DrawElements(gl.TRIANGLES, int32(len(mesh.Indices)), gl.UNSIGNED_INT, nil)
	gl.BindVertexArray(0)
}
*/