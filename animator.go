package gamelib

import (
    "errors"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"
)

const (
	AnimatorPlayInfinitely = -1
)

type animatorNode struct {
	textures       []*Texture
	currentImg     int
	waitTime       int
	timesPlayed    int
	maxTimesPlayed int
	frameStartTime int
	playing        bool
}

type Animator struct {
	states       map[string]*animatorNode
	currentState string
}

func NewAnimator() *Animator {
	animator := new(Animator)
	animator.states = make(map[string]*animatorNode)
	animator.currentState = ""
	return animator
}

// Adds a state to the animator. If maxTimesPlayed is AnimatorPlayInfinitely, it will play infinitely.
func (animator *Animator) AddState(stateName string, textures []*Texture, waitTime, currentImg, maxTimesPlayed int) {
	node := new(animatorNode)
	node.currentImg = currentImg
	node.waitTime = waitTime
	node.frameStartTime = 0
	node.playing = false
	node.maxTimesPlayed = maxTimesPlayed
	node.textures = textures
	animator.states[stateName] = node
}

func (animator *Animator) SetState(stateName string) error {
	if stateName != "" {
		_, ok := animator.states[stateName]
		if !ok {
			return errors.New("state not in animator, maybe you need to add it")
		}
	}
	animator.currentState = stateName
	return nil
}

func (animator *Animator) GetState() string {
	return animator.currentState
}

func (animator *Animator) GetFrame() int {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		return node.currentImg
	}
	return -1
}

func (animator *Animator) GetFrameSize() vector.Vec2 {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		return node.textures[node.currentImg].Size
	}
	return vector.Zero
}

func (animator *Animator) IsPlaying() bool {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		return node.playing
	} else {
		return false
	}
}

func (animator *Animator) Play() error {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		if !node.playing {
			node.playing = true
			node.timesPlayed = 0
			node.frameStartTime = int(sdl.GetTicks())
		}
	} else {
		return errors.New("playing without setting a state")
	}
	return nil
}

func (animator *Animator) Stop() error {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		if node.playing {
			node.playing = false
		}
	} else {
		return errors.New("stopping without setting a state")
	}
	return nil
}

func (animator *Animator) Update() {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		if node.playing {
			if int(sdl.GetTicks()) - node.frameStartTime >= node.waitTime {
				if node.currentImg >= len(node.textures) - 1 {
					node.currentImg = 0
					node.timesPlayed++
				}
				node.currentImg++
				node.frameStartTime = int(sdl.GetTicks())
			}
			if node.timesPlayed > node.maxTimesPlayed && node.maxTimesPlayed != AnimatorPlayInfinitely {
				node.playing = false
			}
		}
	}
}

// Zoom here is a scaling factor. A zoom of zero will result in a single pixel. A zoom of 1 will render the original size.
// Smoothing will do software AA on the surface. This is slower but the image will look better.
func (animator *Animator) Draw(position vector.Vec2, angle, drawSize float32, renderer *sdl.Renderer) error {
	if animator.currentState != "" {
		node := animator.states[animator.currentState]
		center := node.textures[node.currentImg].Size.Copy().DivBy(2)
		size := node.textures[node.currentImg].Size.Copy().MulBy(drawSize)
		return node.textures[node.currentImg].Draw(position, *center, *size, float64(angle), sdl.FLIP_NONE, renderer)
	}
	return nil
}

func (animator *Animator) Free(freeTextures bool) {
	if freeTextures {
		for _, val := range animator.states {
			for i := 0; i < len(val.textures); i++ {
				val.textures[i].Free()
			}
		}
	}
}


