package gamelib

import (
    "errors"
)

type Scene interface {
    Init(data ...interface{})
    GetName() string
    Update(userData interface{}) (interface{}, error)
    Draw(ptr interface{}) error
    ParseEvent(event, userData interface{})
    Reset()
    Free()
}

type SceneManager struct {
    scenes       map[string]Scene
    currentScene string
}

func NewSceneManager() *SceneManager {
    sceneManager := new(SceneManager)
    sceneManager.scenes = make(map[string]Scene)
    return sceneManager
}

func (sceneManager *SceneManager) AddScene(scene Scene) {
    name := scene.GetName()
    sceneManager.scenes[name] = scene
}

func (sceneManager *SceneManager) SetCurrentScene(name string, data ...interface{}) error {
    if name != "" {
        scene, ok := sceneManager.scenes[name]
        if !ok {
            return errors.New("scene is not added")
        }
        scene.Init(data...)
    }
    sceneManager.currentScene = name
    return nil
}

func (sceneManager *SceneManager) RemoveScene(name string) {
    scene, ok := sceneManager.scenes[name]
    if !ok {
        return
    }
    scene.Free()
    delete(sceneManager.scenes, name)
    if sceneManager.currentScene == name {
        sceneManager.currentScene = ""
    }
}

func (sceneManager *SceneManager) Update(userData interface{}) (interface{}, error) {
    if sceneManager.currentScene != "" {
        scene := sceneManager.scenes[sceneManager.currentScene]
        data, err := scene.Update(userData)
        if err != nil {
            return nil, err
        }
        return data, nil
    }
    return nil, nil
}

func (sceneManager *SceneManager) ParseEvent(event, userData interface{}) {
    if sceneManager.currentScene != "" {
        scene := sceneManager.scenes[sceneManager.currentScene]
        scene.ParseEvent(event, userData)
    }
}

func (sceneManager *SceneManager) Draw(ptr interface{}) error {
    if sceneManager.currentScene != "" {
        scene := sceneManager.scenes[sceneManager.currentScene]
        err := scene.Draw(ptr)
        if err != nil {
            return err
        }
    }
    return nil
}

func (sceneManager *SceneManager) Reset() {
    if sceneManager.currentScene != "" {
        scene := sceneManager.scenes[sceneManager.currentScene]
        scene.Reset()
    }
}

func (sceneManager *SceneManager) GetState() string {
    return sceneManager.currentScene
}

func (sceneManager *SceneManager) GetScene(sceneName string) Scene {
    if sceneManager.currentScene != "" {
        return sceneManager.scenes[sceneName]
    }
    return nil
}

func (sceneManager *SceneManager) Free() {
    for key := range sceneManager.scenes {
        sceneManager.RemoveScene(key)
    }
}
