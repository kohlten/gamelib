package gamelib

/*import (
    "fmt"
    "image"
    "image/draw"
    _ "image/jpeg"
    _ "image/png"
    "os"

    "github.com/go-gl/gl/all-core/gl"

    "gitlab.com/kohlten/gamelib/obj"
)

type GlTexture struct {
	Id uint32
	Type string
	Info obj.TextureMap
}

func NewGlTextureFromFile(info obj.TextureMap, texType string) (*GlTexture, error) {
	texture := new(GlTexture)
	texture.Info = info
	texture.Type = texType
	imgFile, err := os.Open(info.Filename)
	if err != nil {
		return nil, fmt.Errorf("texture %q not found on disk: %v", info.Filename, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return nil, err
	}
	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return nil, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)
	gl.GenTextures(1, &texture.Id)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture.Id)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))
	return texture, nil
}
*/