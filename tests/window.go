package main

import (
	"fmt"
	"os"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/veandco/go-sdl2/sdl"

	"gitlab.com/kohlten/gamelib"
)

func testWindowBasic() {
	windowSettings := gamelib.NewWindowSettings()
	windowSettings.Vsync = true
	windowSettings.Height = 400
	windowSettings.Width = 400
	windowSettings.Monitor = 0
	windowSettings.Name = "Window Test"
	window, err := gamelib.NewWindow(windowSettings)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	window.Free()
}

func testWindowOpenGl() {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		panic(err.Error())
	}
	defer sdl.Quit()
	window, err := sdl.CreateWindow("OpenGl Test", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, int32(400), int32(400), sdl.WINDOW_OPENGL)
	if err != nil {
		panic(err.Error())
	}
	defer window.Destroy()
	context, err := window.GLCreateContext()
	if err != nil {
		panic(err.Error())
	}
	defer sdl.GLDeleteContext(context)
	err = gl.Init()
	if err != nil {
		panic(err.Error())
	}
	gl.Enable(gl.DEPTH_TEST)
	gl.ClearColor(1.0, 0.0, 0.0, 1.0)
	gl.ClearDepth(1)
	gl.DepthFunc(gl.LEQUAL)
	gl.Viewport(0, 0, int32(400), int32(400))
	for i := 0; i < 100; i++ {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		}
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		window.GLSwap()
	}
}

func testWindow() {
	testWindowBasic()
	testWindowOpenGl()
}
