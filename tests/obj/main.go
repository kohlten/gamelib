package main

import (
    "errors"
    "fmt"

    "gitlab.com/kohlten/gamelib/obj"
)

var mtl = `
newmtl my_mtl
Ka 0.0435 0.0435 0.0435
Kd 0.1086 0.1086 0.1086
Ks 0.0000 0.0000 0.0000
Tf 0.9885 0.9885 0.9885
illum 6
Ns 10.0000
sharpness 60
Ni 1.19713
map_Ka -s 1 1 1 -o 0 0 0 -mm 0 1 chrome.mpc
map_Kd -s 1 1 1 -o 0 0 0 -mm 0 1 chrome.mpc
map_Ks -s 1 1 1 -o 0 0 0 -mm 0 1 chrome.mpc
map_Ns -s 1 1 1 -o 0 0 0 -mm 0 1 wisp.mps
map_d -s 1 1 1 -o 0 0 0 -mm 0 1 wisp.mps
disp -s 1 1 0.5 wisp.mps
decal -s 1 1 1 -o 0 0 0 -mm 0 1 sand.mps
bump -s 1 1 1 -o 0 0 0 -bm 1 sand.mpb
`

func testBasic() error {
	mtls, err := obj.LoadMtlFromBytes([]byte(mtl))
	if err != nil {
		return err
	}
	for i := 0; i < len(mtls); i++ {
		fmt.Println(mtls[i])
	}
	return nil
}

func testFindNext() error {
	found, location := obj.FindNext([]byte(mtl), []byte("illum"))
	if !found {
		return errors.New("illum not found")
	}
	if location != 237 {
		return errors.New("not at the right location")
	}
	return nil
}

func testCommandLineParser() error {
	parser := obj.NewFlagParser("")
	parser.AddArg("a", false, "-a <bool>")
	parser.AddArg("b", 0.0, "-b <float64>")
	parser.AddArg("c", 0, "-c <int>")
	parser.AddArg("d", int64(0), "-d <int64>")
	parser.AddArg("e", "hello", "-e <string>")
	parser.AddArg("f", 0, "-f <uint>")
	parser.AddArg("g", 0, "-g <uint64>")
	args := []interface{} {
		"hello", 0, uint(0), int64(0), true, 0.0, uint64(0),
	}
	parser.AddMultiValue("h", args, "-h <string> <int> <uint> <int64> <bool> <float> <uint64>")
	commandArgs := []string {
		"-a", "true",
		"-b", "1.0",
		"-c", "1",
		"-d", "1",
		"-e", "goodbye",
		"-f", "1",
		"-g", "1",
		"-h", "n", "1", "1", "1", "false", "0.0", "1",
	}
	err := parser.Parse(commandArgs)
	if err != nil {
		return err
	}
	return nil
}

func testFileMtl() error {
	mtl, err := obj.LoadMtl("Chaynik.mtl")
	if err != nil {
		return err
	}
	fmt.Println(mtl[0])
	return nil
}

func testFileObj() error {
	manager, err := obj.LoadObj("Chaynik.obj")
	if err != nil {
		return err
	}
	fmt.Println(manager.Objs[0])
	return nil
}


func main() {
	//err := testParseVec()
	//if err != nil {
	//	panic(err)
	//}
	//err := testFindNext()
	//if err != nil {
	//	panic(err)
	//}
	//err := testBasic()
	//if err != nil {
	//	panic(err)
	//}
	//err := testCommandLineParser()
	//if err != nil {
	//	panic(err)
	//}
	// err := testBasic()
	//if err != nil {
	//	panic(err)
	//}
	err := testFileObj()
	if err != nil {
		panic(err)
	}
}
