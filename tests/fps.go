package main

import (
    "fmt"
    "os"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib"
)

func createWindow() *gamelib.Window {
	windowSettings := gamelib.NewWindowSettings()
	windowSettings.Height = 400
	windowSettings.Width = 400
	windowSettings.Monitor = 0
	windowSettings.Name = "Fps Test"
	windowSettings.Multisampling = true
	windowSettings.MultisamplingSamples = 32
	windowSettings.Smoothing = true
	window, err := gamelib.NewWindow(windowSettings)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return window
}

func testFpsCounter() {
	window := createWindow()
	fps := gamelib.NewFpsCounter()
	running := true
	timer := gamelib.NewTimer(3000, gamelib.TimeMilliseconds)
	timer.Start()
	for running {
		startTime := gamelib.GetTimeNS()
		for {
			event := sdl.PollEvent()
			if event == nil {
				break
			}
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
			}
		}
		err := window.Renderer.SetDrawColor(0, 0, 0, 255)
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		err = window.Renderer.Clear()
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		window.Renderer.Present()
		gamelib.LimitFPS(startTime, 60)
		fps.Update()
		timer.Update()
		fmt.Println(fps.GetFps())
		if timer.IsCompleted() {
			running = false
		}
	}
	window.Free()
}
