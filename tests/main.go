package main

import (
	"runtime"
)

func init() {
	runtime.LockOSThread()
}

func main() {
	testConfig()
	testWindow()
	testButton()
	testFpsCounter()
	testText()
	testAnimator()
}
