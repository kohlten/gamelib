package main

import (
    "fmt"
    "os"
    "strconv"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib"
    "gitlab.com/kohlten/gamelib/vector"
)

func testText() {
	window := createWindow()
	text, err := gamelib.NewTextFromJson("assets/fasttracker2-style_12x12.json", window.Renderer)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	fps := gamelib.NewFpsCounter()
	running := true
	timer := gamelib.NewTimer(10000, gamelib.TimeMilliseconds)
	timer.Start()
	for running {
		startTime := gamelib.GetTimeNS()
		for {
			event := sdl.PollEvent()
			if event == nil {
				break
			}
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
			}
		}
		fps.Update()
		err := window.Renderer.SetDrawColor(0, 0, 0, 255)
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		err = window.Renderer.Clear()
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		stringFps := strconv.Itoa(int(fps.GetFps()))
		textSize := text.GetSize(stringFps)
		windowSize := window.GetSize()
		_ = text.Draw(stringFps, vector.NewVec2(windowSize.X / 2 - textSize.X / 2, windowSize.Y / 2 - textSize.Y / 2), vector.Zero, 0, nil, window.Renderer)
		window.Renderer.Present()
		gamelib.LimitFPS(startTime, 144)
		timer.Update()
		if timer.IsCompleted() {
			running = false
		}
	}
	window.Free()
}
