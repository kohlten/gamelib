package main

import (
    "fmt"

    "gitlab.com/kohlten/gamelib/ecs"
)

type Position struct {
	x, y int
}

type Bounce struct {
}

func (b *Bounce) Init(system *ecs.System, data ...interface{}) {

}

func (b *Bounce) Update(system *ecs.System, dt int, data ...interface{}) {
	entities := system.GetEntities()
	for i := 0; i < len(entities); i++ {
		entityHandle := ecs.NewEntityHandle(entities[i], system.GetParent())
		position, _ := entityHandle.GetComponent("position")
		position.Data.(*Position).x += 1
		position.Data.(*Position).y += 1
		fmt.Println(position.Data.(*Position).x, position.Data.(*Position).y)
	}
}

func (b *Bounce) Render(system *ecs.System, data ...interface{}) {

}

func main() {
	world := ecs.NewWorld()
	_ = world.AddSystem(ecs.NewSystem(&Bounce{}), "bounce")
	world.Init()
	ball := world.CreateEntity()
	_ = world.AddEntityToSystem(ball.GetEntity(), "bounce")
	_ = ball.AddComponent(&Position{0, 0}, "position")
	_ = world.RemoveEntity(ball)
	_ = world.AddEntity(ball)
	_ = world.AddEntityToSystem(ball.GetEntity(), "bounce")
	for i := 0; i < 50; i++ {
		world.Update(20)
	}
}
