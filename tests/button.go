package main

import (
    "fmt"
    "os"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"

    "gitlab.com/kohlten/gamelib"
)

func testButton() {
	window := createWindow()
	fps := gamelib.NewFpsCounter()
	running := true
	timer := gamelib.NewTimer(10000, gamelib.TimeMilliseconds)
	timer.Start()
	text, err:= gamelib.NewTextFromJson("assets/fasttracker2-style_12x12.json", window.Renderer)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	buttonRegular, err := gamelib.NewTextureFromFile("assets/button.png", window.Renderer)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	buttonHover, err := gamelib.NewTextureFromFile("assets/button_hover.png", window.Renderer)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	windowSize := window.GetSize()
	button := gamelib.NewButton(text, *windowSize.DivBy(2), vector.NewVec2(222.0, 39.0), buttonRegular, buttonHover, nil)
	button.SetString("Press me!")
	for running {
		startTime := gamelib.GetTimeNS()
		for {
			event := sdl.PollEvent()
			if event == nil {
				break
			}
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
			}
		}
		fps.Update()
		timer.Update()
		button.Update()
		err := window.Renderer.SetDrawColor(0, 255, 255, 255)
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		err = window.Renderer.Clear()
		if err != nil {
			fmt.Println(err.Error())
			running = false
		}
		_ = button.Draw(0, window.Renderer)
		window.Renderer.Present()
		gamelib.LimitFPS(startTime, 144)
		if timer.IsCompleted() || button.IsPressed() {
			running = false
		}
	}
	window.Free()
}