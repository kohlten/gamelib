package main

import (
    "fmt"
    "math/rand"
    "os"
    "reflect"
    "time"

    "gitlab.com/kohlten/gamelib/db"
)

const header = "Database error: "

func testDump() bool {
	database, err := db.NewDatabase("test.data", "COPY", db.DefaultMaxNodesPerPage)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	err = database.SetValue("test0", 0)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	err = database.SetValue("test1", 1)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	err = database.Dump()
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	database, err = db.LoadDatabase("test.data")
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	return true
}

func testSet() bool {
	database, err := db.NewDatabase("test.data", "COPY", db.DefaultMaxNodesPerPage)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	err = database.SetValue("test0", 0)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	err = database.SetValue("test1", 1)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	value, t, err := database.GetValue("test0")
	if err != nil || t != "int" || value.(int) != 0 {
		fmt.Println(header, err)
		return false
	}
	value, t, err = database.GetValue("test1")
	if err != nil || t != "int" || value.(int) != 1 {
		fmt.Println(header, err)
		return false
	}
	err = database.Dump()
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	database, err = db.LoadDatabase("test.data")
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	value, t, err = database.GetValue("test0")
	if err != nil || t != "int" || value.(int) != 0 {
		fmt.Println(header, err)
		return false
	}
	value, t, err = database.GetValue("test1")
	if err != nil || t != "int" || value.(int) != 1 {
		fmt.Println(header, err)
		return false
	}
	return true
}

var source = rand.NewSource(time.Now().UnixNano())
var randSource = rand.New(source)

func createString(length int) string {
	data := make([]byte, length)
	for i := 0; i < length; i++ {
		data[i] = byte(randSource.Intn(256))
	}
	return string(data)
}

func createKey(length int) string {
	data := make([]byte, length)
	for i := 0; i < length; i++ {
		data[i] = byte(randSource.Intn(35) + 97)
	}
	return string(data)
}

func createInt() int {
	return randSource.Int()
}

func createFloat() float32 {
	return randSource.Float32()
}

func testLarge() bool {
	database, err := db.NewDatabase("test.data", "GZIP", 100)
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	vars := 500
	keys := make([]string, vars)
	values := make([]interface{}, vars)
	for i := 0; i < vars; i++ {
		key := createKey(5)
		var data interface{}
		if i % 10 == 0 && i > 0 {
			data = createString(50)
		} else {
			if i % 2 == 0 && i > 0 {
				data = createInt()
			} else {
				data = createFloat()
			}
		}
		keys[i] = key
		values[i] = data
		err = database.SetValue(key, data)
		if err != nil {
			fmt.Println(header, err.Error())
			return false
		}
	}
	err = database.Dump()
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	database, err = db.LoadDatabase("test.data")
	if err != nil {
		fmt.Println(header, err.Error())
		return false
	}
	for i := 0; i < vars; i++ {
		data, t, err := database.GetValue(keys[i])
		if err != nil {
			fmt.Println(header, err.Error())
			return false
		}
		if t != reflect.ValueOf(data).Type().String() || data != values[i] {
			fmt.Println(header, data, " != ", values[i])
			return false
		}
	}
	return true
}

func main() {
	if !testDump() {
		os.Exit(1)
	}
	if !testSet() {
		os.Exit(1)
	}
	if !testLarge() {
		os.Exit(1)
	}
}
