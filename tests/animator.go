package main

import (
    "fmt"
    "os"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"

    "gitlab.com/kohlten/gamelib"
)

type Game struct {
	window *gamelib.Window
	animator *gamelib.Animator
	images []*gamelib.Surface
	running bool
}

func (game *Game) createWindow() {
	windowSettings := gamelib.NewWindowSettings()
	windowSettings.Vsync = true
	windowSettings.Height = 400
	windowSettings.Width = 400
	windowSettings.Monitor = 0
	windowSettings.Name = "Animator Test"
	window, err := gamelib.NewWindow(windowSettings)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	game.window = window
	game.running = true
}

func (game *Game) createAnimator() {
	game.animator = gamelib.NewAnimator()
	spriteSheet, err := gamelib.NewSurfaceFromFile("assets/character_squished.png")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	images, err := gamelib.SplitSpriteSheet(spriteSheet, vector.Vec2{93, 105})
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	imageTexs := make([]*gamelib.Texture, len(images))
	for i := 0; i < len(images); i++ {
		imageTexs[i], err = gamelib.NewTextureFromSurface(images[i], game.window.Renderer)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		images[i].Free()
	}
	game.images = images
	game.animator.AddState("default", imageTexs, 120, 0, 1)
	game.animator.SetState("default")
	game.animator.Play()
}

func (game *Game) loop() {
	for game.running {
		for {
			event := sdl.PollEvent()
			if event == nil {
				break
			}
			switch event.(type) {
			case *sdl.QuitEvent:
				game.running = false
			}
		}
		err := game.window.Renderer.SetDrawColor(0, 0, 0, 255)
		if err != nil {
			fmt.Println(err.Error())
			game.running = false
		}
		err = game.window.Renderer.Clear()
		if err != nil {
			fmt.Println(err.Error())
			game.running = false
		}
		game.animator.Update()
		if !game.animator.IsPlaying() {
			game.running = false
		}
		frameSize := game.animator.GetFrameSize()
		windowSize := game.window.GetSize()
		position := vector.NewVec2(windowSize.X / 2 - frameSize.X / 2, windowSize.Y / 2 - frameSize.Y / 2)
		_ = game.animator.Draw(position, 0, game.window.Renderer)
		game.window.Renderer.Present()
	}
}

func testAnimator() {
	game := new(Game)
	game.createWindow()
	game.createAnimator()
	game.loop()
	game.window.Free()
	game.animator.Free(true)
}