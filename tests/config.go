package main

import (
    "fmt"
    "math/rand"
    "os"
    "strconv"
    "time"

    "gitlab.com/kohlten/gamelib"
)

func createNewKey(length int) []byte {
	key := make([]byte, length)
	for i := 0; i < length; i++ {
		key[i] = byte(rand.Intn(26) + 97)
	}
	return key
}

func createNewBool() []byte {
	val := rand.Intn(2)
	if val == 1 {
		return []byte("TRUE")
	}
	return []byte("FALSE")
}

func createNewNum() []byte {
	chance := rand.Intn(2)
	if chance == 1 {
		return []byte(strconv.FormatFloat(float64(rand.Int()), 'f', -1, 64))
	} else {
		return []byte(strconv.FormatFloat(rand.Float64(), 'f', -1, 64))
	}
}

func createNewVector(length int) []byte {
	bytes := make([]byte, 0)
	for i := 0; i < length; i++ {
		val := []byte(strconv.FormatFloat(rand.Float64(), 'f', -1, 64))
		bytes = append(bytes, val...)
		if i < length - 1 {
			bytes = append(bytes, ',')
		}
	}
	return bytes
}

func createConfig() []byte {
	rand.Seed(time.Now().UnixNano())
	configValue := make([]byte, 0)
	size := rand.Intn(500) + 50
	for i := 0; i < size; i++ {
		valueType := rand.Intn(3)
		key := createNewKey(rand.Intn(10) + 5)
		configValue = append(configValue, key...)
		configValue = append(configValue, ' ')
		switch valueType {
		case gamelib.ConfigBool:
			val := createNewBool()
			configValue = append(configValue, val...)
		case gamelib.ConfigNum:
			val := createNewNum()
			configValue = append(configValue, val...)
		case gamelib.ConfigVector:
			val := createNewVector(rand.Intn(5) + 1)
			configValue = append(configValue, val...)
		}
		configValue = append(configValue, '\n')
	}
	return configValue
}

func basicTest() {
	configValue := createConfig()
	config, err := gamelib.ParseConfigString(string(configValue))
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	err = config.WriteToFile("test.config")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	config, err = gamelib.ParseConfig("test.config")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	os.Remove("test.config")
}

func testConfig() {
	basicTest()
}