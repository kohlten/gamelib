package gamelib

import (
    "errors"

    "github.com/veandco/go-sdl2/sdl"

    "gitlab.com/kohlten/gamelib/vector"
)

type Texture struct {
    tex  *sdl.Texture
    Size vector.Vec2
}

func NewTextureFromSurface(surface *Surface, renderer *sdl.Renderer) (*Texture, error) {
    texture := new(Texture)
    texture.Size = surface.Size
    tex, err := renderer.CreateTextureFromSurface(surface.surf)
    if err != nil {
        return nil, err
    }
    texture.tex = tex
    return texture, err
}

func NewTextureFromFile(name string, renderer *sdl.Renderer) (*Texture, error) {
    surf, err := NewSurfaceFromFile(name)
    if err != nil {
        return nil, err
    }
    texture, err := NewTextureFromSurface(surf, renderer)
    if err != nil {
        return nil, err
    }
    surf.Free()
    return texture, nil
}

func NewTexture(size vector.Vec2, format, access int, renderer *sdl.Renderer) (*Texture, error) {
    texture := new(Texture)
    tex, err := renderer.CreateTexture(uint32(format), access, int32(size.X), int32(size.Y))
    if err != nil {
        return nil, err
    }
    texture.tex = tex
    texture.Size = size
    err = texture.Clear(renderer)
    if err != nil {
        return nil, err
    }
    return texture, nil
}

func (texture *Texture) SetColor(color sdl.Color) error {
    err := texture.tex.SetColorMod(color.R, color.G, color.B)
    if err != nil {
        return err
    }
    return texture.tex.SetAlphaMod(color.A)
}

func (texture *Texture) SetBlend(mode sdl.BlendMode) error {
    return texture.tex.SetBlendMode(mode)
}

func (texture *Texture) SetPixel(position vector.Vec2, color sdl.Color) error {
    if position.X < 0 || position.Y < 0 || position.X > texture.Size.X || position.Y > texture.Size.Y {
        return errors.New("position greater than the size or less than zero")
    }
    pixels, pitch, err := texture.tex.Lock(nil)
    if err != nil {
        return err
    }
    formatInt, _, _, _, err := texture.tex.Query()
    if err != nil {
        texture.tex.Unlock()
        return err
    }
    format, err := sdl.AllocFormat(uint(formatInt))
    if err != nil {
        texture.tex.Unlock()
        return err
    }
    location := int32(position.Y)*(int32(pitch)/4) + int32(position.X)
    intColor := sdl.MapRGBA(format, color.R, color.G, color.B, color.A)
    pixels[location] = byte(intColor & 0xff)
    pixels[location+1] = byte((intColor >> 8) & 0xff)
    pixels[location+2] = byte((intColor >> 16) & 0xff)
    pixels[location+3] = byte((intColor >> 24) & 0xff)
    err = texture.tex.Update(nil, pixels, pitch)
    if err != nil {
        format.Free()
        texture.tex.Unlock()
        return err
    }
    format.Free()
    texture.tex.Unlock()
    return nil
}

func (texture *Texture) Clear(renderer *sdl.Renderer) error {
    pixels, pitch, err := texture.tex.Lock(nil)
    if err != nil {
        return err
    }
    for i := 0; i < len(pixels); i++ {
        pixels[i] = 0
    }
    err = texture.tex.Update(nil, pixels, pitch)
    texture.tex.Unlock()
    if err != nil {
        return err
    }
    return nil
}

// Center decides the point to rotate around.
// Clip decides the rectangle in which you want to be shown.
func (texture *Texture) Draw(position, center, outputSize vector.Vec2, angle float64, flip sdl.RendererFlip, renderer *sdl.Renderer) error {
    quad := new(sdl.Rect)
    quad.X = int32(position.X)
    quad.Y = int32(position.Y)
    if !outputSize.Equal(&vector.Zero) {
        quad.W = int32(outputSize.X)
        quad.H = int32(outputSize.Y)
    } else {
        quad.W = int32(texture.Size.X)
        quad.H = int32(texture.Size.Y)
    }
    err := renderer.CopyEx(texture.tex, nil, quad, angle, &sdl.Point{X: int32(center.X), Y: int32(center.Y)}, flip)
    return err
}

func (texture *Texture) Resize(size vector.Vec2) {

}

func (texture *Texture) Free() error {
    return texture.tex.Destroy()
}
